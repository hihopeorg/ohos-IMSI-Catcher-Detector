# ohos-IMSI-Catcher-Detector

**本项目是基于开源项目IMSI-Catcher-Detector进行ohos化的移植和开发的，可以通过项目标签以及github地址( https://github.com/CellularPrivacy/Android-IMSI-Catcher-Detector )追踪到原项目版本**

#### 项目介绍

- 项目名称：开源IMSI捕获器检测器
- 所属系列：ohos的第三方组件适配移植
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/CellularPrivacy/Android-IMSI-Catcher-Detector
- 原项目基线版本：v0.1.43-alpha ,sha1: c4d06cf7fe7b20bf81585bc6ec67e515a633e0b4
- 功能介绍：AIMSICD是一个用于检测IMSI-Catchers的ohos应用。这些设备是在目标移动电话与服务提供商的真实塔之间起作用的虚假移动塔（基站）AIMSICD尝试通过以下检测方法来检测IMSI捕获器：
- 1.	检查手机信息一致性
- 2.	检查LAC /小区ID一致性
- 3.	检查相邻小区信息
- 4.	防止无提示应用程序安装
- 5.	监控信号强度
- 6.	检测无提示短信
- 7.	检测毫微微小区
- 编程语言：Java
- 外部依赖：无

#### 效果展示

<img src="screenshot/效果展示.gif"/>

#### 安装教程
方法1.

1.下载依赖库har包ohos-IMSI-Catcher-Detector.har。
2.启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3.在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```
4.在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'io.realm.ohos:realm:1.0.1'
}
```

#### 使用说明

1.获取设备信息：
```
    //卡1 : 0 ；卡2 :1 卡槽索引号，取值范围为0〜设备支持的最大卡槽索引号
    int slotld = 0;
    // 获取RadioInfoManager对象。
    RadioInfoManager radioInfoManager = RadioInfoManager.getInstance(this);

    //获取注册网络中CS域的RAT。
    RadioInfoManagerUtils managerUtils = new RadioInfoManagerUtils(this);
    String phoneType = managerUtils.getPhoneType(slotld);
```
2.主要函数功能说明：
```
    SimInfoManager：来获取 SIM 卡的相关信息，需要注意，要添加对应的权限
    NetManager.getInstance(Context)获取网络管理的实例对象
    NetManager.getDefaultNet()获取默认的数据网络。
```
3.根据实际需要，蜂窝网络
```
    /**
    * 配置一个彩信类型的蜂窝网络
    */
    NetSpecifier req = new NetSpecifier.Builder()
        .addCapability(NetCapabilities.NET_CAPABILITY_MMS)
        .addBearer(NetCapabilities.BEARER_CELLULAR)
        .build();
```
#### 版本迭代

- v1.0.1

#### 版权和许可信息

GNU General Public License v3.0