//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package io.realm.internal;

import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.log.RealmLog;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Util {
    public Util() {
    }

    public static long getNativeMemUsage() {
        return nativeGetMemUsage();
    }

    static native long nativeGetMemUsage();

    static void javaPrint(String txt) {
    }

    public static String getTablePrefix() {
        return nativeGetTablePrefix();
    }

    static native String nativeGetTablePrefix();

    public static Class<? extends RealmModel> getOriginalModelClass(Class<? extends RealmModel> clazz) {
        Class<? extends RealmModel> superclass = (Class<? extends RealmModel>) clazz.getSuperclass();
        if (!superclass.equals(Object.class) && !superclass.equals(RealmObject.class)) {
            clazz = superclass;
        }

        return clazz;
    }

    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }

    public static boolean isEmulator() {
//        return Build.FINGERPRINT.startsWith("generic") || Build.FINGERPRINT.startsWith("unknown") || Build.MODEL.contains("google_sdk") || Build.MODEL.contains("Emulator") || Build.MODEL.contains("ohos SDK built for x86") || Build.MANUFACTURER.contains("Genymotion") || Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic") || "google_sdk".equals(Build.PRODUCT);
        return true;
    }

    public static boolean deleteRealm(String canonicalPath, File realmFolder, String realmFileName) {
        boolean realmDeleted = true;
        String management = ".management";
        File managementFolder = new File(realmFolder, realmFileName + ".management");
        File[] files = managementFolder.listFiles();
        if (files != null) {
            File[] var7 = files;
            int var8 = files.length;

            for(int var9 = 0; var9 < var8; ++var9) {
                File file = var7[var9];
                realmDeleted = realmDeleted && file.delete();
            }
        }

        realmDeleted = realmDeleted && managementFolder.delete();
        return realmDeleted && deletes(canonicalPath, realmFolder, realmFileName);
    }

    private static boolean deletes(String canonicalPath, File rootFolder, String realmFileName) {
        AtomicBoolean realmDeleted = new AtomicBoolean(true);
        List<File> filesToDelete = Arrays.asList(new File(rootFolder, realmFileName), new File(rootFolder, realmFileName + ".lock"), new File(rootFolder, realmFileName + ".log_a"), new File(rootFolder, realmFileName + ".log_b"), new File(rootFolder, realmFileName + ".log"), new File(canonicalPath));
        Iterator var5 = filesToDelete.iterator();

        while(var5.hasNext()) {
            File fileToDelete = (File)var5.next();
            if (fileToDelete.exists()) {
                boolean deleteResult = fileToDelete.delete();
                if (!deleteResult) {
                    realmDeleted.set(false);
                    RealmLog.warn("Could not delete the file %s", new Object[]{fileToDelete});
                }
            }
        }

        return realmDeleted.get();
    }
}
