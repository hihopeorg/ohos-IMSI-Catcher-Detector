//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public interface Row {
    long getColumnCount();

    String getColumnName(long var1);

    long getColumnIndex(String var1);

    RealmFieldType getColumnType(long var1);

    Table getTable();

    long getIndex();

    long getLong(long var1);

    boolean getBoolean(long var1);

    float getFloat(long var1);

    double getDouble(long var1);

    Date getDate(long var1);

    String getString(long var1);

    byte[] getBinaryByteArray(long var1);

    long getLink(long var1);

    boolean isNullLink(long var1);

    LinkView getLinkList(long var1);

    void setLong(long var1, long var3);

    void setBoolean(long var1, boolean var3);

    void setFloat(long var1, float var3);

    void setDouble(long var1, double var3);

    void setDate(long var1, Date var3);

    void setString(long var1, String var3);

    void setBinaryByteArray(long var1, byte[] var3);

    void setLink(long var1, long var3);

    void nullifyLink(long var1);

    boolean isNull(long var1);

    void setNull(long var1);

    boolean isAttached();

    boolean hasColumn(String var1);
}
