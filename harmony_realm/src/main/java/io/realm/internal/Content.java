package io.realm.internal;

import java.lang.ref.ReferenceQueue;

class Context {
    private static final ReferenceQueue<NativeObject> referenceQueue = new ReferenceQueue();
    private static final Thread finalizingThread;
    static final Context dummyContext;

    public Context() {
    }

    void addReference(NativeObject referent) {
        new NativeObjectReference(this, referent, referenceQueue);
    }

    static {
        finalizingThread = new Thread(new FinalizerRunnable(referenceQueue));
        dummyContext = new Context();
        finalizingThread.setName("RealmFinalizingDaemon");
        finalizingThread.start();
    }
}