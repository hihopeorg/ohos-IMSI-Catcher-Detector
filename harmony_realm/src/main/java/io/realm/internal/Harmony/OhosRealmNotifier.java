package io.realm.internal.Harmony;


import io.realm.internal.Capabilities;
import io.realm.internal.RealmNotifier;
import io.realm.internal.SharedRealm;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;


/**
 * {@link RealmNotifier} implementation for Ohos.
 */
public class OhosRealmNotifier extends RealmNotifier {
    private EventHandler eventHandler;
   // EventHandler对应Handler InnerEvent对应Message EventRunner对应Looper
    public OhosRealmNotifier(SharedRealm sharedRealm, Capabilities capabilities) {
        super(sharedRealm);
        if (capabilities.canDeliverNotification()) {
            EventRunner runner = EventRunner.create();
//            handler = new Handler(Looper.myLooper());
            eventHandler= new EventHandler(runner);
        } else {
            eventHandler = null;
        }
    }

    @Override
    public boolean post(Runnable runnable) {
        return eventHandler != null && eventHandler.hasInnerEvent(runnable);
    }
}
