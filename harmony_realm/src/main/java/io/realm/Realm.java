
package io.realm;

import io.realm.OsRealmSchema.Creator;
import io.realm.RealmConfiguration.Builder;
import io.realm.annotations.TargetApi;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmFileException;
import io.realm.exceptions.RealmFileException.Kind;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.*;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.SharedRealm.VersionID;
import io.realm.internal.async.RealmAsyncTaskImpl;
import io.realm.log.RealmLog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONException;
import ohos.utils.zson.ZSONObject;
import ohos.utils.zson.ZSONReader;
import rx.Observable;

import java.io.IOException;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public class Realm extends BaseRealm {
    public static final String DEFAULT_REALM_NAME = "default.realm";
    private static RealmConfiguration defaultConfiguration;

    Realm(RealmConfiguration configuration) {
        super(configuration);
    }

    public Observable<Realm> asObservable() {
        return this.configuration.getRxFactory().from(this);
    }

    public static synchronized void init(Context context) {
        if (BaseRealm.applicationContext == null) {
            if (context == null) {
                throw new IllegalArgumentException("Non-null context required.");
            }

            RealmCore.loadLibrary(context);
            defaultConfiguration = (new Builder(context)).build();
            ObjectServerFacade.getSyncFacadeIfPossible().init(context);
            BaseRealm.applicationContext = context.getApplicationContext();
            SharedRealm.initialize(new File(context.getFilesDir() + ".realm.temp"));
        }

    }

    public static Realm getDefaultInstance() {
        if (defaultConfiguration == null) {
            throw new IllegalStateException("Call `Realm.init(Context)` before calling this method.");
        } else {
            return (Realm) RealmCache.createRealmOrGetFromCache(defaultConfiguration, Realm.class);
        }
    }

    public static Realm getInstance(RealmConfiguration configuration) {
        if (configuration == null) {
            throw new IllegalArgumentException("A non-null RealmConfiguration must be provided");
        } else {
            return (Realm) RealmCache.createRealmOrGetFromCache(configuration, Realm.class);
        }
    }

    public static void setDefaultConfiguration(RealmConfiguration configuration) {
        if (configuration == null) {
            throw new IllegalArgumentException("A non-null RealmConfiguration must be provided");
        } else {
            defaultConfiguration = configuration;
        }
    }

    public static void removeDefaultConfiguration() {
        defaultConfiguration = null;
    }

    static Realm createInstance(RealmConfiguration configuration, ColumnIndices[] globalCacheArray) {
        try {
            return createAndValidate(configuration, globalCacheArray);
        } catch (RealmMigrationNeededException var5) {
            RealmMigrationNeededException e = var5;
            if (configuration.shouldDeleteRealmIfMigrationNeeded()) {
                deleteRealm(configuration);
            } else {
                try {
                    if (configuration.getMigration() != null) {
                        migrateRealm(configuration, e);
                    }
                } catch (FileNotFoundException var4) {
                    throw new RealmFileException(Kind.NOT_FOUND, var4);
                }
            }

            return createAndValidate(configuration, globalCacheArray);
        }
    }

    private static Realm createAndValidate(RealmConfiguration configuration, ColumnIndices[] globalCacheArray) {
        Realm realm = new Realm(configuration);
        long currentVersion = realm.getVersion();
        long requiredVersion = configuration.getSchemaVersion();
        ColumnIndices columnIndices = RealmCache.findColumnIndices(globalCacheArray, requiredVersion);
        if (columnIndices != null) {
            realm.schema.setInitialColumnIndices(columnIndices);
        } else {
            boolean syncingConfig = configuration.isSyncConfiguration();
            if (!syncingConfig && currentVersion != -1L) {
                if (currentVersion < requiredVersion) {
                    realm.doClose();
                    throw new RealmMigrationNeededException(configuration.getPath(), String.format("Realm on disk need to migrate from v%s to v%s", currentVersion, requiredVersion));
                }

                if (requiredVersion < currentVersion) {
                    realm.doClose();
                    throw new IllegalArgumentException(String.format("Realm on disk is newer than the one specified: v%s vs. v%s", currentVersion, requiredVersion));
                }
            }

            try {
                if (!syncingConfig) {
                    initializeRealm(realm);
                } else {
                    initializeSyncedRealm(realm);
                }
            } catch (RuntimeException var10) {
                realm.doClose();
                throw var10;
            }
        }

        return realm;
    }

    private static void initializeRealm(Realm realm) {
        boolean commitChanges = false;

        try {
            realm.beginTransaction();
            long currentVersion = realm.getVersion();
            boolean unversioned = currentVersion == -1L;
            commitChanges = unversioned;
            RealmConfiguration configuration = realm.getConfiguration();
            if (unversioned) {
                realm.setVersion(configuration.getSchemaVersion());
            }

            RealmProxyMediator mediator = configuration.getSchemaMediator();
            Set<Class<? extends RealmModel>> modelClasses = mediator.getModelClasses();
            if (unversioned) {
                Iterator var8 = modelClasses.iterator();

                while (var8.hasNext()) {
                    Class<? extends RealmModel> modelClass = (Class) var8.next();
                    mediator.createRealmObjectSchema(modelClass, realm.getSchema());
                }
            }

            Map<Class<? extends RealmModel>, ColumnInfo> columnInfoMap = new HashMap(modelClasses.size());
            Iterator var17 = modelClasses.iterator();

            while (var17.hasNext()) {
                Class<? extends RealmModel> modelClass = (Class) var17.next();
                columnInfoMap.put(modelClass, mediator.validateTable(modelClass, realm.sharedRealm, false));
            }

            realm.getSchema().setInitialColumnIndices(unversioned ? configuration.getSchemaVersion() : currentVersion, columnInfoMap);
            if (unversioned) {
                Realm.Transaction transaction = configuration.getInitialDataTransaction();
                if (transaction != null) {
                    transaction.execute(realm);
                }
            }
        } catch (Exception var14) {
            commitChanges = false;
            throw var14;
        } finally {
            if (commitChanges) {
                realm.commitTransaction();
            } else {
                realm.cancelTransaction();
            }

        }

    }

    private static void initializeSyncedRealm(Realm realm) {
        boolean commitChanges = false;
        OsRealmSchema schema = null;

        try {
            realm.beginTransaction();
            long currentVersion = realm.getVersion();
            boolean unversioned = currentVersion == -1L;
            RealmConfiguration configuration = realm.getConfiguration();
            RealmProxyMediator mediator = configuration.getSchemaMediator();
            Set<Class<? extends RealmModel>> modelClasses = mediator.getModelClasses();
            Creator schemaCreator = new Creator();
            Iterator var10 = modelClasses.iterator();

            while (var10.hasNext()) {
                Class<? extends RealmModel> modelClass = (Class) var10.next();
                mediator.createRealmObjectSchema(modelClass, schemaCreator);
            }

            schema = new OsRealmSchema(schemaCreator);
            long newVersion = configuration.getSchemaVersion();
            long schemaNativePointer = schema.getNativePtr();
            if (realm.sharedRealm.requiresMigration(schemaNativePointer)) {
                if (currentVersion >= newVersion) {
                    throw new IllegalArgumentException(String.format("The schema was changed but the schema version was not updated. The configured schema version (%d) must be greater than the version  in the Realm file (%d) in order to update the schema.", newVersion, currentVersion));
                }

                realm.sharedRealm.updateSchema(schemaNativePointer, newVersion);
                realm.setVersion(newVersion);
                commitChanges = true;
            }

            Map<Class<? extends RealmModel>, ColumnInfo> columnInfoMap = new HashMap(modelClasses.size());
            Iterator var15 = modelClasses.iterator();

            while (var15.hasNext()) {
                Class<? extends RealmModel> modelClass = (Class) var15.next();
                columnInfoMap.put(modelClass, mediator.validateTable(modelClass, realm.sharedRealm, false));
            }

            realm.getSchema().setInitialColumnIndices(unversioned ? newVersion : currentVersion, columnInfoMap);
            if (unversioned) {
                Realm.Transaction transaction = configuration.getInitialDataTransaction();
                if (transaction != null) {
                    transaction.execute(realm);
                }
            }
        } catch (Exception var20) {
            commitChanges = false;
            throw var20;
        } finally {
            if (schema != null) {
                schema.close();
            }

            if (commitChanges) {
                realm.commitTransaction();
            } else {
                realm.cancelTransaction();
            }

        }

    }

    public <E extends RealmModel> void createAllFromJson(Class<E> clazz, ZSONArray json) {
        if (clazz != null && json != null) {
            this.checkIfValid();

            for (int i = 0; i < json.size(); ++i) {
                try {
                    this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, json.getZSONObject(i), false);
                } catch (ZSONException var5) {
                    throw new RealmException("Could not map JSON", var5);
                }
            }

        }
    }

    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> clazz, ZSONArray json) {
        if (clazz != null && json != null) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);

            for (int i = 0; i < json.size(); ++i) {
                try {
                    this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, json.getZSONObject(i), true);
                } catch (ZSONException var5) {
                    throw new RealmException("Could not map JSON", var5);
                }
            }

        }
    }

    public <E extends RealmModel> void createAllFromJson(Class<E> clazz, String json) {
        if (clazz != null && json != null && json.length() != 0) {
            ZSONArray arr;
            try {
                arr = new ZSONArray(Collections.singletonList(json));
            } catch (ZSONException var5) {
                throw new RealmException("Could not create JSON array from string", var5);
            }

            this.createAllFromJson(clazz, arr);
        }
    }

    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> clazz, String json) {
        if (clazz != null && json != null && json.length() != 0) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);

            ZSONArray arr;
            try {
                arr = new ZSONArray(Collections.singletonList(json));
            } catch (ZSONException var5) {
                throw new RealmException("Could not create JSON array from string", var5);
            }

            this.createOrUpdateAllFromJson(clazz, arr);
        }
    }

    @TargetApi(11)
    public <E extends RealmModel> void createAllFromJson(Class<E> clazz, InputStream inputStream) throws IOException {
        if (clazz != null && inputStream != null) {
            this.checkIfValid();
            ZSONReader reader = new ZSONReader(new InputStreamReader(inputStream, "UTF-8"));

            try {
                reader.startArray();

                while (reader.hasNext()) {
                    this.configuration.getSchemaMediator().createUsingJsonStream(clazz, this, reader);
                }

                reader.endArray();
            } finally {
                reader.close();
            }
        }
    }

    @TargetApi(11)
    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> clazz, InputStream in) {
        if (clazz != null && in != null) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);
            Scanner scanner = null;

            try {
                scanner = this.getFullStringScanner(in);
                ZSONArray json = ZSONArray.stringToZSONArray(scanner.next());

                for (int i = 0; i < json.size(); ++i) {
                    this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, json.getZSONObject(i), true);
                }
            } catch (ZSONException var9) {
                throw new RealmException("Failed to read JSON", var9);
            } finally {
                if (scanner != null) {
                    scanner.close();
                }

            }

        }
    }

    public <E extends RealmModel> E createObjectFromJson(Class<E> clazz, ZSONObject json) {
        if (clazz != null && json != null) {
            this.checkIfValid();

            try {
                return this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, json, false);
            } catch (ZSONException var4) {
                throw new RealmException("Could not map JSON", var4);
            }
        } else {
            return null;
        }
    }

    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> clazz, ZSONObject json) {
        if (clazz != null && json != null) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);

            try {
                return this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, json, true);
            } catch (ZSONException var4) {
                throw new RealmException("Could not map JSON", var4);
            }
        } else {
            return null;
        }
    }

    public <E extends RealmModel> E createObjectFromJson(Class<E> clazz, String json) {
        if (clazz != null && json != null && json.length() != 0) {
            ZSONObject obj = null;
            try {
                ZSONObject.stringToZSON(json);
            } catch (ZSONException var5) {
                throw new RealmException("Could not create Json object from string", var5);
            }

            return this.createObjectFromJson(clazz, obj);
        } else {
            return null;
        }
    }

    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> clazz, String json) {
        if (clazz != null && json != null && json.length() != 0) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);

            ZSONObject obj = null;
            try {
                obj = ZSONObject.stringToZSON(json);
            } catch (ZSONException var5) {
                throw new RealmException("Could not create Json object from string", var5);
            }

            return this.createOrUpdateObjectFromJson(clazz, obj);
        } else {
            return null;
        }
    }

    @TargetApi(11)
    public <E extends RealmModel> E createObjectFromJson(Class<E> clazz, InputStream inputStream) throws IOException {
        if (clazz != null && inputStream != null) {
            this.checkIfValid();
            Table table = this.schema.getTable(clazz);
            RealmModel realmObject;
            if (table.hasPrimaryKey()) {
                Scanner scanner = null;

                try {
                    scanner = this.getFullStringScanner(inputStream);
//                    ZSONObject json = new ZSONObject.stringToZSON(scanner.next());

                    realmObject = this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(clazz, this, ZSONObject.stringToZSON(scanner.next()), false);
                } catch (ZSONException var16) {
                    throw new RealmException("Failed to read JSON", var16);
                } finally {
                    if (scanner != null) {
                        scanner.close();
                    }

                }
            } else {
                ZSONReader reader = new ZSONReader(new InputStreamReader(inputStream, "UTF-8"));

                try {
                    realmObject = this.configuration.getSchemaMediator().createUsingJsonStream(clazz, this, reader);
                } finally {
                    reader.close();
                }
            }

            return (E) realmObject;
        } else {
            return null;
        }
    }

    @TargetApi(11)
    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> clazz, InputStream in) {
        if (clazz != null && in != null) {
            this.checkIfValid();
            this.checkHasPrimaryKey(clazz);
            Scanner scanner = null;

            RealmModel var5;
            try {
                scanner = this.getFullStringScanner(in);
                ZSONObject json = ZSONObject.stringToZSON(scanner.next());
                var5 = this.createOrUpdateObjectFromJson(clazz, json);
            } catch (ZSONException var9) {
                throw new RealmException("Failed to read JSON", var9);
            } finally {
                if (scanner != null) {
                    scanner.close();
                }

            }

            return (E) var5;
        } else {
            return null;
        }
    }

    private Scanner getFullStringScanner(InputStream in) {
        return (new Scanner(in, "UTF-8")).useDelimiter("\\A");
    }

    public <E extends RealmModel> E createObject(Class<E> clazz) {
        this.checkIfValid();
        return this.createObjectInternal(clazz, true, Collections.emptyList());
    }

    <E extends RealmModel> E createObjectInternal(Class<E> clazz, boolean acceptDefaultValue, List<String> excludeFields) {
        Table table = this.schema.getTable(clazz);
        if (table.hasPrimaryKey()) {
            throw new RealmException(String.format("'%s' has a primary key, use 'createObject(Class<E>, Object)' instead.", Table.tableNameToClassName(table.getName())));
        } else {
            long rowIndex = table.addEmptyRow();
            return this.get(clazz, rowIndex, acceptDefaultValue, excludeFields);
        }
    }

    public <E extends RealmModel> E createObject(Class<E> clazz, Object primaryKeyValue) {
        this.checkIfValid();
        return this.createObjectInternal(clazz, primaryKeyValue, true, Collections.emptyList());
    }

    <E extends RealmModel> E createObjectInternal(Class<E> clazz, Object primaryKeyValue, boolean acceptDefaultValue, List<String> excludeFields) {
        Table table = this.schema.getTable(clazz);
        long rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue);
        return this.get(clazz, rowIndex, acceptDefaultValue, excludeFields);
    }

    public <E extends RealmModel> E copyToRealm(E object) {
        this.checkNotNullObject(object);
        return (E) this.copyOrUpdate(object, false, new HashMap());
    }

    public <E extends RealmModel> E copyToRealmOrUpdate(E object) {
        this.checkNotNullObject(object);
        this.checkHasPrimaryKey(object.getClass());
        return this.copyOrUpdate(object, true, (Map<RealmModel, RealmObjectProxy>) new HashMap());
    }

    public <E extends RealmModel> List<E> copyToRealm(Iterable<E> objects) {
        if (objects == null) {
            return new ArrayList();
        } else {
            Map<RealmModel, RealmObjectProxy> cache = new HashMap();
            ArrayList<E> realmObjects = new ArrayList();
            Iterator var4 = objects.iterator();

            while (var4.hasNext()) {
                E object = (E) var4.next();
                this.checkNotNullObject(object);
                realmObjects.add(this.copyOrUpdate(object, false, cache));
            }

            return realmObjects;
        }
    }

    public void insert(Collection<? extends RealmModel> objects) {
        this.checkIfValidAndInTransaction();
        if (objects == null) {
            throw new IllegalArgumentException("Null objects cannot be inserted into Realm.");
        } else if (!objects.isEmpty()) {
            this.configuration.getSchemaMediator().insert(this, objects);
        }
    }

    public void insert(RealmModel object) {
        this.checkIfValidAndInTransaction();
        if (object == null) {
            throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
        } else {
            Map<RealmModel, Long> cache = new HashMap();
            this.configuration.getSchemaMediator().insert(this, object, cache);
        }
    }

    public void insertOrUpdate(Collection<? extends RealmModel> objects) {
        this.checkIfValidAndInTransaction();
        if (objects == null) {
            throw new IllegalArgumentException("Null objects cannot be inserted into Realm.");
        } else if (!objects.isEmpty()) {
            this.configuration.getSchemaMediator().insertOrUpdate(this, objects);
        }
    }

    public void insertOrUpdate(RealmModel object) {
        this.checkIfValidAndInTransaction();
        if (object == null) {
            throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
        } else {
            Map<RealmModel, Long> cache = new HashMap();
            this.configuration.getSchemaMediator().insertOrUpdate(this, object, cache);
        }
    }

    public <E extends RealmModel> List<E> copyToRealmOrUpdate(Iterable<E> objects) {
        if (objects == null) {
            return new ArrayList(0);
        } else {
            Map<RealmModel, RealmObjectProxy> cache = new HashMap();
            ArrayList<E> realmObjects = new ArrayList();
            Iterator var4 = objects.iterator();

            while (var4.hasNext()) {
                E object = (E) var4.next();
                this.checkNotNullObject(object);
                realmObjects.add(this.copyOrUpdate(object, true, cache));
            }

            return realmObjects;
        }
    }

    public <E extends RealmModel> List<E> copyFromRealm(Iterable<E> realmObjects) {
        return this.copyFromRealm(realmObjects, 2147483647);
    }

    public <E extends RealmModel> List<E> copyFromRealm(Iterable<E> realmObjects, int maxDepth) {
        this.checkMaxDepth(maxDepth);
        if (realmObjects == null) {
            return new ArrayList(0);
        } else {
            ArrayList<E> unmanagedObjects = new ArrayList();
            Map<RealmModel, CacheData<RealmModel>> listCache = new HashMap();
            Iterator var5 = realmObjects.iterator();

            while (var5.hasNext()) {
                E object = (E) var5.next();
                this.checkValidObjectForDetach(object);
                unmanagedObjects.add(this.createDetachedCopy(object, maxDepth, listCache));
            }

            return unmanagedObjects;
        }
    }

    public <E extends RealmModel> E copyFromRealm(E realmObject) {
        return this.copyFromRealm(realmObject, 2147483647);
    }

    public <E extends RealmModel> E copyFromRealm(E realmObject, int maxDepth) {
        this.checkMaxDepth(maxDepth);
        this.checkValidObjectForDetach(realmObject);
        return this.createDetachedCopy(realmObject, maxDepth, (Map<RealmModel, CacheData<RealmModel>>) new HashMap());
    }

    public <E extends RealmModel> RealmQuery<E> where(Class<E> clazz) {
        this.checkIfValid();
        return RealmQuery.createQuery(this, clazz);
    }

    public void addChangeListener(RealmChangeListener<Realm> listener) {
        this.addListener(listener);
    }

    public void removeChangeListener(RealmChangeListener<Realm> listener) {
        this.removeListener(listener);
    }

    public void removeAllChangeListeners() {
        this.removeAllListeners();
    }

    public void executeTransaction(Realm.Transaction transaction) {
        if (transaction == null) {
            throw new IllegalArgumentException("Transaction should not be null");
        } else {
            this.beginTransaction();

            try {
                transaction.execute(this);
                this.commitTransaction();
            } catch (Throwable var3) {
                if (this.isInTransaction()) {
                    this.cancelTransaction();
                } else {
                    RealmLog.warn("Could not cancel transaction, not currently in a transaction.", new Object[0]);
                }

                throw var3;
            }
        }
    }

    public RealmAsyncTask executeTransactionAsync(Realm.Transaction transaction) {
        return this.executeTransactionAsync(transaction, (Realm.Transaction.OnSuccess) null, (Realm.Transaction.OnError) null);
    }

    public RealmAsyncTask executeTransactionAsync(Realm.Transaction transaction, Realm.Transaction.OnSuccess onSuccess) {
        if (onSuccess == null) {
            throw new IllegalArgumentException("onSuccess callback can't be null");
        } else {
            return this.executeTransactionAsync(transaction, onSuccess, (Realm.Transaction.OnError) null);
        }
    }

    public RealmAsyncTask executeTransactionAsync(Realm.Transaction transaction, Realm.Transaction.OnError onError) {
        if (onError == null) {
            throw new IllegalArgumentException("onError callback can't be null");
        } else {
            return this.executeTransactionAsync(transaction, (Realm.Transaction.OnSuccess) null, onError);
        }
    }

    public RealmAsyncTask executeTransactionAsync(final Realm.Transaction transaction, final Realm.Transaction.OnSuccess onSuccess, final Realm.Transaction.OnError onError) {
        this.checkIfValid();
        if (transaction == null) {
            throw new IllegalArgumentException("Transaction should not be null");
        } else {
            final boolean canDeliverNotification = this.sharedRealm.capabilities.canDeliverNotification();
            if (onSuccess != null || onError != null) {
                this.sharedRealm.capabilities.checkCanDeliverNotification("Callback cannot be delivered on current thread.");
            }

            final RealmConfiguration realmConfiguration = this.getConfiguration();
            final RealmNotifier realmNotifier = this.sharedRealm.realmNotifier;
            Future<?> pendingTransaction = asyncTaskExecutor.submitTransaction(new Runnable() {
                public void run() {
                    if (!Thread.currentThread().isInterrupted()) {
                        VersionID versionID = null;
                        Throwable exception = null;
                        Realm bgRealm = Realm.getInstance(realmConfiguration);
                        bgRealm.beginTransaction();

                        try {
                            transaction.execute(bgRealm);
                            if (Thread.currentThread().isInterrupted()) {
                                return;
                            }

                            bgRealm.commitTransaction();
                            versionID = bgRealm.sharedRealm.getVersionID();
                        } catch (Throwable var40) {
                            exception = var40;
                        } finally {
                            try {
                                if (bgRealm.isInTransaction()) {
                                    bgRealm.cancelTransaction();
                                }
                            } finally {
                                bgRealm.close();
                            }

                        }

                        if (canDeliverNotification) {
                            if (versionID != null && onSuccess != null) {
                                VersionID finalVersionID = versionID;
                                realmNotifier.post(new Runnable() {
                                    public void run() {
                                        if (Realm.this.isClosed()) {
                                            onSuccess.onSuccess();
                                        } else {
                                            if (Realm.this.sharedRealm.getVersionID().compareTo(finalVersionID) < 0) {
                                                Realm.this.sharedRealm.realmNotifier.addTransactionCallback(new Runnable() {
                                                    public void run() {
                                                        onSuccess.onSuccess();
                                                    }
                                                });
                                            } else {
                                                onSuccess.onSuccess();
                                            }

                                        }
                                    }
                                });
                            } else if (exception != null) {
                                Throwable finalException = exception;
                                realmNotifier.post(new Runnable() {
                                    public void run() {
                                        if (onError != null) {
                                            onError.onError(finalException);
                                        } else {
                                            throw new RealmException("Async transaction failed", finalException);
                                        }
                                    }
                                });
                            }
                        } else if (exception != null) {
                            throw new RealmException("Async transaction failed", exception);
                        }

                    }
                }
            });
            return new RealmAsyncTaskImpl(pendingTransaction, asyncTaskExecutor);
        }
    }

    public void delete(Class<? extends RealmModel> clazz) {
        this.checkIfValid();
        this.schema.getTable(clazz).clear();
    }

    private <E extends RealmModel> E copyOrUpdate(E object, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        this.checkIfValid();
        return this.configuration.getSchemaMediator().copyOrUpdate(this, object, update, cache);
    }

    private <E extends RealmModel> E createDetachedCopy(E object, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        this.checkIfValid();
        return this.configuration.getSchemaMediator().createDetachedCopy(object, maxDepth, cache);
    }

    private <E extends RealmModel> void checkNotNullObject(E object) {
        if (object == null) {
            throw new IllegalArgumentException("Null objects cannot be copied into Realm.");
        }
    }

    private void checkHasPrimaryKey(Class<? extends RealmModel> clazz) {
        if (!this.schema.getTable(clazz).hasPrimaryKey()) {
            throw new IllegalArgumentException("A RealmObject with no @PrimaryKey cannot be updated: " + clazz.toString());
        }
    }

    private void checkMaxDepth(int maxDepth) {
        if (maxDepth < 0) {
            throw new IllegalArgumentException("maxDepth must be > 0. It was: " + maxDepth);
        }
    }

    private <E extends RealmModel> void checkValidObjectForDetach(E realmObject) {
        if (realmObject == null) {
            throw new IllegalArgumentException("Null objects cannot be copied from Realm.");
        } else if (RealmObject.isManaged(realmObject) && RealmObject.isValid(realmObject)) {
            if (realmObject instanceof DynamicRealmObject) {
                throw new IllegalArgumentException("DynamicRealmObject cannot be copied from Realm.");
            }
        } else {
            throw new IllegalArgumentException("Only valid managed objects can be copied from Realm.");
        }
    }

    public static void migrateRealm(RealmConfiguration configuration) throws FileNotFoundException {
        migrateRealm(configuration, (RealmMigration) null);
    }

    private static void migrateRealm(RealmConfiguration configuration, RealmMigrationNeededException cause) throws FileNotFoundException {
        BaseRealm.migrateRealm(configuration, (RealmMigration) null, new MigrationCallback() {
            public void migrationComplete() {
            }
        }, cause);
    }

    public static void migrateRealm(RealmConfiguration configuration, RealmMigration migration) throws FileNotFoundException {
        BaseRealm.migrateRealm(configuration, migration, new MigrationCallback() {
            public void migrationComplete() {
            }
        }, (RealmMigrationNeededException) null);
    }

    public static boolean deleteRealm(RealmConfiguration configuration) {
        return BaseRealm.deleteRealm(configuration);
    }

    public static boolean compactRealm(RealmConfiguration configuration) {
        if (configuration.isSyncConfiguration()) {
            throw new UnsupportedOperationException("Compacting is not supported yet on synced Realms.");
        } else {
            return BaseRealm.compactRealm(configuration);
        }
    }

    Table getTable(Class<? extends RealmModel> clazz) {
        return this.schema.getTable(clazz);
    }

    ColumnIndices updateSchemaCache(ColumnIndices[] globalCacheArray) {
        long currentSchemaVersion = this.sharedRealm.getSchemaVersion();
        long cacheSchemaVersion = this.schema.getSchemaVersion();
        if (currentSchemaVersion == cacheSchemaVersion) {
            return null;
        } else {
            ColumnIndices createdGlobalCache = null;
            RealmProxyMediator mediator = this.getConfiguration().getSchemaMediator();
            ColumnIndices cacheForCurrentVersion = RealmCache.findColumnIndices(globalCacheArray, currentSchemaVersion);
            if (cacheForCurrentVersion == null) {
                Set<Class<? extends RealmModel>> modelClasses = mediator.getModelClasses();
                HashMap map = new HashMap(modelClasses.size());

                try {
                    Iterator var11 = modelClasses.iterator();

                    while (var11.hasNext()) {
                        Class<? extends RealmModel> clazz = (Class) var11.next();
                        ColumnInfo columnInfo = mediator.validateTable(clazz, this.sharedRealm, true);
                        map.put(clazz, columnInfo);
                    }
                } catch (RealmMigrationNeededException var14) {
                    throw var14;
                }

                cacheForCurrentVersion = createdGlobalCache = new ColumnIndices(currentSchemaVersion, map);
            }

            this.schema.updateColumnIndices(cacheForCurrentVersion, mediator);
            return createdGlobalCache;
        }
    }

    public static Object getDefaultModule() {
        String moduleName = "io.realm.DefaultRealmModule";

        try {
            Class<?> clazz = Class.forName(moduleName);
            Constructor<?> constructor = clazz.getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            return constructor.newInstance();
        } catch (ClassNotFoundException var3) {
            return null;
        } catch (InvocationTargetException var4) {
            throw new RealmException("Could not create an instance of " + moduleName, var4);
        } catch (InstantiationException var5) {
            throw new RealmException("Could not create an instance of " + moduleName, var5);
        } catch (IllegalAccessException var6) {
            throw new RealmException("Could not create an instance of " + moduleName, var6);
        }
    }

    public static int getGlobalInstanceCount(RealmConfiguration configuration) {
        final AtomicInteger globalCount = new AtomicInteger(0);
        RealmCache.invokeWithGlobalRefCount(configuration, new io.realm.RealmCache.Callback() {
            public void onResult(int count) {
                globalCount.set(count);
            }
        });
        return globalCount.get();
    }

    public static int getLocalInstanceCount(RealmConfiguration configuration) {
        return RealmCache.getLocalThreadCount(configuration);
    }

    public interface Transaction {
        void execute(Realm var1);

        public interface OnError {
            void onError(Throwable var1);
        }

        public interface OnSuccess {
            void onSuccess();
        }

        public static class Callback {
            public Callback() {
            }

            public void onSuccess() {
            }

            public void onError(Exception ignore) {
            }
        }
    }
}
