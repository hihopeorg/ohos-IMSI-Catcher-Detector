//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package io.realm;

import io.realm.internal.Collection;
import io.realm.internal.Row;
import io.realm.internal.SortDescriptor;
import io.realm.internal.Table;
import io.realm.internal.UncheckedRow;
import rx.Observable;

public class RealmResults<E extends RealmModel> extends OrderedRealmCollectionImpl<E> {
    static <T extends RealmModel> RealmResults<T> createBacklinkResults(BaseRealm realm, Row row, Class<T> srcTableType, String srcFieldName) {
        if (!(row instanceof UncheckedRow)) {
            throw new IllegalArgumentException("Row is " + row.getClass());
        } else {
            UncheckedRow uncheckedRow = (UncheckedRow)row;
            Table srcTable = realm.getSchema().getTable(srcTableType);
            return new RealmResults(realm, Collection.createBacklinksCollection(realm.sharedRealm, uncheckedRow, srcTable, srcFieldName), srcTableType);
        }
    }

    RealmResults(BaseRealm realm, Collection collection, Class<E> clazz) {
        super(realm, collection, clazz);
    }

    RealmResults(BaseRealm realm, Collection collection, String className) {
        super(realm, collection, className);
    }

    public RealmQuery<E> where() {
        this.realm.checkIfValid();
        return RealmQuery.createQueryFromResult(this);
    }

    public RealmResults<E> sort(String fieldName1, Sort sortOrder1, String fieldName2, Sort sortOrder2) {
        return this.sort(new String[]{fieldName1, fieldName2}, new Sort[]{sortOrder1, sortOrder2});
    }

    public boolean isLoaded() {
        this.realm.checkIfValid();
        return this.collection.isLoaded();
    }

    public boolean load() {
        this.realm.checkIfValid();
        this.collection.load();
        return true;
    }

    public void addChangeListener(RealmChangeListener<RealmResults<E>> listener) {
        this.checkForAddRemoveListener(listener, true);
        this.collection.addListener(this, listener);
    }

    public void addChangeListener(OrderedRealmCollectionChangeListener<RealmResults<E>> listener) {
        this.checkForAddRemoveListener(listener, true);
        this.collection.addListener(this, listener);
    }

    private void checkForAddRemoveListener(Object listener, boolean checkListener) {
        if (checkListener && listener == null) {
            throw new IllegalArgumentException("Listener should not be null");
        } else {
            this.realm.checkIfValid();
            this.realm.sharedRealm.capabilities.checkCanDeliverNotification("Listeners cannot be used on current thread.");
        }
    }

    public void removeAllChangeListeners() {
        this.checkForAddRemoveListener((Object)null, false);
        this.collection.removeAllListeners();
    }

    /** @deprecated */
    @Deprecated
    public void removeChangeListeners() {
        this.removeAllChangeListeners();
    }

    public void removeChangeListener(RealmChangeListener<RealmResults<E>> listener) {
        this.checkForAddRemoveListener(listener, true);
        this.collection.removeListener(this, listener);
    }

    public void removeChangeListener(OrderedRealmCollectionChangeListener<RealmResults<E>> listener) {
        this.checkForAddRemoveListener(listener, true);
        this.collection.removeListener(this, listener);
    }

    public Observable<RealmResults<E>> asObservable() {
        if (this.realm instanceof Realm) {
            return this.realm.configuration.getRxFactory().from((Realm)this.realm, this);
        } else if (this.realm instanceof DynamicRealm) {
            DynamicRealm dynamicRealm = (DynamicRealm)this.realm;
            Observable results = this.realm.configuration.getRxFactory().from(dynamicRealm, (RealmResults<DynamicRealmObject>) this);
            return results;
        } else {
            throw new UnsupportedOperationException(this.realm.getClass() + " does not support RxJava.");
        }
    }

    /** @deprecated */
    @Deprecated
    public RealmResults<E> distinct(String fieldName) {
        SortDescriptor distinctDescriptor = SortDescriptor.getInstanceForDistinct(this.collection.getTable(), fieldName);
        Collection distinctCollection = this.collection.distinct(distinctDescriptor);
        return this.createLoadedResults(distinctCollection);
    }

    /** @deprecated */
    @Deprecated
    public RealmResults<E> distinctAsync(String fieldName) {
        return this.where().distinctAsync(fieldName);
    }

    /** @deprecated */
    @Deprecated
    public RealmResults<E> distinct(String firstFieldName, String... remainingFieldNames) {
        return this.where().distinct(firstFieldName, remainingFieldNames);
    }
}
