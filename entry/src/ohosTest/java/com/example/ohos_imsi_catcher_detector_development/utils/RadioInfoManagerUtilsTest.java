package com.example.ohos_imsi_catcher_detector_development.utils;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SignalInformation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class RadioInfoManagerUtilsTest {

    private RadioInfoManagerUtils radioInfoManagerUtils;
    private Context appContext;
    private int slot_ld = 0;//默认卡槽1
    private RadioInfoManager radioInfoManager;


    @Before
    public void setUp() throws Exception {
        appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        radioInfoManagerUtils = new RadioInfoManagerUtils(appContext);
        radioInfoManager = RadioInfoManager.getInstance(appContext);
    }

    @Test
    public void getNetworkState() {
        String networkState = radioInfoManagerUtils.getNetworkState(slot_ld);
        Assert.assertEquals("It's right", "Disconnected", networkState);
    }

    @Test
    public void getPhoneType() {
        int networkSelectionMode = radioInfoManager.getNetworkSelectionMode(slot_ld);
        Assert.assertEquals("It's right", 0, networkSelectionMode);
    }

    @Test
    public void getIsoCountryCodeForNetwork() {
        String isoCountryCodeForNetwork = radioInfoManager.getIsoCountryCodeForNetwork(slot_ld);
        Assert.assertEquals("It's right", "cn", isoCountryCodeForNetwork);
    }

    @Test
    public void getOperatorName() {
        String operatorName = radioInfoManager.getOperatorName(slot_ld);
        Assert.assertEquals("It's right", "华为内网", operatorName);
    }

    @Test
    public void getPlmnNumeric() {
//      序列号 46060
        String plmnNumeric = radioInfoManager.getPlmnNumeric(slot_ld);
        Assert.assertEquals("It's right", "46060", plmnNumeric);
    }

    @Test
    public void isRoaming() {
        boolean isRoaming = radioInfoManager.isRoaming(slot_ld);
        System.out.println("isRoaming==" + isRoaming);
        Assert.assertEquals("It's right", false, isRoaming);
    }

    @Test
    public void getNetworkSelectionMode() {
        int networkSelectionMode = radioInfoManager.getNetworkSelectionMode(slot_ld);
        Assert.assertEquals("It's right", 0, networkSelectionMode);
    }

    @Test
    public void getSignalInfoList() {
        List<SignalInformation> signalInfoList = radioInfoManager.getSignalInfoList(slot_ld);
        Assert.assertEquals("It's right", 1, signalInfoList.size());
    }

    @Test
    public void getPrimarySlotId() {
        int primarySlotId = radioInfoManager.getPrimarySlotId();
        Assert.assertEquals("It's right", 0, primarySlotId);
    }

    @Test
    public void getImei() {
        String imei = radioInfoManager.getImei(slot_ld);
        Assert.assertEquals("It's right", "", imei);
    }

}