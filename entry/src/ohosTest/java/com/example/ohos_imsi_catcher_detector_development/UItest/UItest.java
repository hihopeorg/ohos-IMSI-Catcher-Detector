package com.example.ohos_imsi_catcher_detector_development.UItest;

import com.example.ohos_imsi_catcher_detector_development.Device;
import com.example.ohos_imsi_catcher_detector_development.ListItem;
import com.example.ohos_imsi_catcher_detector_development.MainAbility;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.utils.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import org.junit.Assert;
import org.junit.Test;

public class UItest {

    @Test
    public void startMainItemUi() throws InterruptedException {

        Ability ability = EventHelper.startAbility(MainAbility.class);

        ListContainer listContainer = (ListContainer) ability.findComponentById(ResourceTable.Id_left_drawer_lit);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(0));

        Thread.sleep(3000);
        Assert.assertNotNull("It's right", listContainer);
    }

    @Test
    public void startItemUi() throws InterruptedException {

        Ability ability = EventHelper.startAbility(Device.class);
        Text txt_list = (Text) ability.findComponentById(ResourceTable.Id_txt_list);

        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, txt_list);

        Thread.sleep(3000);
        Assert.assertNotNull("It's right", txt_list);
    }

    @Test
    public void startListUi() throws InterruptedException {


        Ability ability = EventHelper.startAbility(ListItem.class);

        ListContainer listContainer = (ListContainer) ability.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(1));

        Thread.sleep(3000);
        Assert.assertNotNull("It's right", listContainer);
    }
}