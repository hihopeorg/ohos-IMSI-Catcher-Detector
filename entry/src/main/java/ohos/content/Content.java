package ohos.content;

import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.ability.startsetting.AbilityStartSetting;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.app.IAbilityManager;
import ohos.app.ProcessInfo;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.bundle.*;
import ohos.global.configuration.Configuration;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.solidxml.Pattern;
import ohos.global.resource.solidxml.Theme;
import ohos.utils.net.Uri;

import java.io.File;

public class Content implements Context {
    @Override
    public TaskDispatcher createParallelTaskDispatcher(String s, TaskPriority taskPriority) {
        return null;
    }

    @Override
    public TaskDispatcher createSerialTaskDispatcher(String s, TaskPriority taskPriority) {
        return null;
    }

    @Override
    public TaskDispatcher getGlobalTaskDispatcher(TaskPriority taskPriority) {
        return null;
    }

    @Override
    public TaskDispatcher getMainTaskDispatcher() {
        return null;
    }

    @Override
    public TaskDispatcher getUITaskDispatcher() {
        return null;
    }

    @Override
    public ApplicationInfo getApplicationInfo() {
        return null;
    }

    @Override
    public ProcessInfo getProcessInfo() {
        return null;
    }

    @Override
    public AbilityInfo getAbilityInfo() {
        return null;
    }

    @Override
    public ResourceManager getResourceManager() {
        return null;
    }

    @Override
    public File getPreferencesDir() {
        return null;
    }

    @Override
    public File getDatabaseDir() {
        return null;
    }

    @Override
    public File getDistributedDir() {
        return null;
    }

    @Override
    public void switchToDeviceEncryptedStorageContext() {

    }

    @Override
    public void switchToCredentialEncryptedStorageContext() {

    }

    @Override
    public boolean isDeviceEncryptedStorage() {
        return false;
    }

    @Override
    public boolean isCredentialEncryptedStorage() {
        return false;
    }

    @Override
    public int verifyCallingPermission(String s) {
        return 0;
    }

    @Override
    public int verifySelfPermission(String s) {
        return 0;
    }

    @Override
    public int verifyCallingOrSelfPermission(String s) {
        return 0;
    }

    @Override
    public int verifyPermission(String s, int i, int i1) {
        return 0;
    }

    @Override
    public ClassLoader getClassloader() {
        return null;
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void terminateAbility() {

    }

    @Override
    public void terminateAbility(int i) {

    }

    @Override
    public void displayUnlockMissionMessage() {

    }

    @Override
    public void lockMission() {

    }

    @Override
    public void unlockMission() {

    }

    @Override
    public String getLocalClassName() {
        return null;
    }

    @Override
    public ElementName getElementName() {
        return null;
    }

    @Override
    public ElementName getCallingAbility() {
        return null;
    }

    @Override
    public String getCallingBundle() {
        return null;
    }

    @Override
    public boolean stopAbility(Intent intent) {
        return false;
    }

    @Override
    public void startAbility(Intent intent, int i) {

    }

    @Override
    public void startAbility(Intent intent, int i, AbilityStartSetting abilityStartSetting) {

    }

    @Override
    public void startAbilities(Intent[] intents) {

    }

    @Override
    public Context createBundleContext(String s, int i) {
        return null;
    }

    @Override
    public boolean canRequestPermission(String s) {
        return false;
    }

    @Override
    public void requestPermissionsFromUser(String[] strings, int i) {

    }

    @Override
    public boolean connectAbility(Intent intent, IAbilityConnection iAbilityConnection) {
        return false;
    }

    @Override
    public void disconnectAbility(IAbilityConnection iAbilityConnection) {

    }

    @Override
    public void setDisplayOrientation(AbilityInfo.DisplayOrientation displayOrientation) {

    }

    @Override
    public IBundleManager getBundleManager() {
        return null;
    }

    @Override
    public String getBundleName() {
        return null;
    }

    @Override
    public String getBundleCodePath() {
        return null;
    }

    @Override
    public String getBundleResourcePath() {
        return null;
    }

    @Override
    public File getDataDir() {
        return null;
    }

    @Override
    public File getCacheDir() {
        return null;
    }

    @Override
    public File getCodeCacheDir() {
        return null;
    }

    @Override
    public File[] getExternalMediaDirs() {
        return new File[0];
    }

    @Override
    public File getNoBackupFilesDir() {
        return null;
    }

    @Override
    public File getFilesDir() {
        return null;
    }

    @Override
    public File getDir(String s, int i) {
        return null;
    }

    @Override
    public File getExternalCacheDir() {
        return null;
    }

    @Override
    public File[] getExternalCacheDirs() {
        return new File[0];
    }

    @Override
    public File getExternalFilesDir(String s) {
        return null;
    }

    @Override
    public File[] getExternalFilesDirs(String s) {
        return new File[0];
    }

    @Override
    public boolean deleteFile(String s) {
        return false;
    }

    @Override
    public IAbilityManager getAbilityManager() {
        return null;
    }

    @Override
    public boolean terminateAbilityResult(int i) {
        return false;
    }

    @Override
    public int getDisplayOrientation() {
        return 0;
    }

    @Override
    public void setShowOnLockScreen(boolean b) {

    }

    @Override
    public void setWakeUpScreen(boolean b) {

    }

    @Override
    public void restart() {

    }

    @Override
    public void setTransitionAnimation(int i, int i1) {

    }

    @Override
    public boolean isUpdatingConfigurations() {
        return false;
    }

    @Override
    public void setTheme(int i) {

    }

    @Override
    public Theme getTheme() {
        return null;
    }

    @Override
    public int getThemeId() {
        return 0;
    }

    @Override
    public void setPattern(int i) {

    }

    @Override
    public Pattern getPattern() {
        return null;
    }

    @Override
    public String getAppType() {
        return null;
    }

    @Override
    public ResourceManager getResourceManager(Configuration configuration) {
        return null;
    }

    @Override
    public Object getLastStoredDataWhenConfigChanged() {
        return null;
    }

    @Override
    public void printDrawnCompleted() {

    }

    @Override
    public void compelVerifyPermission(String s, String s1) {

    }

    @Override
    public void compelVerifyUriPermission(Uri uri, int i, String s) {

    }

    @Override
    public void compelVerifyCallerPermission(String s, String s1) {

    }

    @Override
    public void compelVerifyCallerUriPermission(Uri uri, int i, String s) {

    }

    @Override
    public void compelVerifyPermission(String s, int i, int i1, String s1) {

    }

    @Override
    public void compelVerifyUriPermission(Uri uri, int i, int i1, int i2, String s) {

    }

    @Override
    public void compelVerifyUriPermission(Uri uri, String s, String s1, int i, int i1, int i2, String s2) {

    }

    @Override
    public int getColor(int i) {
        return 0;
    }

    @Override
    public String getString(int i) {
        return null;
    }

    @Override
    public String getString(int i, Object... objects) {
        return null;
    }

    @Override
    public String[] getStringArray(int i) {
        return new String[0];
    }

    @Override
    public int[] getIntArray(int i) {
        return new int[0];
    }

    @Override
    public String getProcessName() {
        return null;
    }

    @Override
    public Context getAbilityPackageContext() {
        return null;
    }

    @Override
    public HapModuleInfo getHapModuleInfo() {
        return null;
    }

    @Override
    public Uri getCaller() {
        return null;
    }

    @Override
    public void setColorMode(int i) {

    }

    @Override
    public int getColorMode() {
        return 0;
    }
}
