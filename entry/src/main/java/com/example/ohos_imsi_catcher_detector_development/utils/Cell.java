/* Ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.utils;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.app.Context;
import ohos.miscservices.timeutility.Time;
import ohos.telephony.TelephonyConstants;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

public class Cell implements Sequenceable {

    public static final String INVALID_PSC = "invalid";

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    /**
     * Current Cell ID
     * Cell Identification code
     */
    private int cellId;

    public int getLocationAreaCode() {
        return locationAreaCode;
    }

    public void setLocationAreaCode(int locationAreaCode) {
        this.locationAreaCode = locationAreaCode;
    }

    public int getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(int mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public int getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public void setMobileNetworkCode(int mobileNetworkCode) {
        this.mobileNetworkCode = mobileNetworkCode;
    }

    /**
     * Location Area Code
     */
    private int locationAreaCode;

    /**
     * Mobile Country Code
     */
    private int mobileCountryCode;

    /**
     * Mobile Network Code
     */
    private int mobileNetworkCode;

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    /**
     * [dBm] RX signal "power"
     * Signal Strength Measurement (dBm)
     */
    private int dbm;

    public int getPrimaryScramblingCode() {
        return primaryScramblingCode;
    }

    /**
     * Primary Scrambling Code
     */
    private int primaryScramblingCode;

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    /**
     * Relative Signal Strength Indicator [dBm, asu etc.]
     * Received Signal Strength Indicator (RSSI)
     */
    private int rssi;

    /**
     * Timing Advance [LTE,GSM]
     * LTE Timing Advance or Integer.MAX_VALUE if unavailable
     */
    private int timingAdvance;

    /**
     * Cell-ID for [CDMA]
     * <p>
     * CDMA System ID
     *
     * @return System ID or Integer.MAX_VALUE if not supported
     */
    private int sid;

    /**
     * Timestamp of current cell information
     */
    private long timestamp;

    // Tracked Cell Specific Variables

    public int getNetType() {
        return netType;
    }

    public void setNetType(int netType) {
        this.netType = netType;
    }

    /**
     * Current Network Type
     */
    private int netType;
    /**
     * Current ground speed in metres/second
     */
    private double speed;
    /**
     * Location accuracy in metres or 0.0 if unavailable
     */
    private double accuracy;
    private double bearing;

    public static String getInvalidPsc() {
        return INVALID_PSC;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * Longitude Geolocation
     */
    private double lon;

    /**
     * Latitude
     */
    private double lat;

    {
        cellId = Integer.MAX_VALUE;
        locationAreaCode = Integer.MAX_VALUE;
        mobileCountryCode = Integer.MAX_VALUE;
        mobileNetworkCode = Integer.MAX_VALUE;
        dbm = Integer.MAX_VALUE;
        primaryScramblingCode = Integer.MAX_VALUE;
        rssi = Integer.MAX_VALUE;
        timingAdvance = Integer.MAX_VALUE;
        sid = Integer.MAX_VALUE;
        netType = Integer.MAX_VALUE;
        lon = 0.0;
        lat = 0.0;
        speed = 0.0;
        accuracy = 0.0;
        bearing = 0.0;
    }

    public Cell() {
    }

    public Cell(int CID, int locationAreaCode, int mobileCountryCode, int mobileNetworkCode, int dbm, long timestamp) {
        super();
        this.cellId = CID;
        this.locationAreaCode = locationAreaCode;
        this.mobileCountryCode = mobileCountryCode;
        this.mobileNetworkCode = mobileNetworkCode;
        this.dbm = dbm;
        this.rssi = Integer.MAX_VALUE;
        this.primaryScramblingCode = Integer.MAX_VALUE;
        this.timestamp = timestamp;
        this.timingAdvance = Integer.MAX_VALUE;
        this.sid = Integer.MAX_VALUE;
        this.netType = Integer.MAX_VALUE;
        this.lon = 0.0;
        this.lat = 0.0;
        this.speed = 0.0;
        this.accuracy = 0.0;
        this.bearing = 0.0;
    }

    public Cell(int CID, int locationAreaCode, int signal, int primaryScramblingCode, int netType, boolean dbm) {
        this.cellId = CID;
        this.locationAreaCode = locationAreaCode;
        this.mobileCountryCode = Integer.MAX_VALUE;
        this.mobileNetworkCode = Integer.MAX_VALUE;

        if (dbm) {
            this.dbm = signal;
        } else {
            this.rssi = signal;
        }
        this.primaryScramblingCode = primaryScramblingCode;

        this.netType = netType;
        this.timingAdvance = Integer.MAX_VALUE;
        this.sid = Integer.MAX_VALUE;
        this.lon = 0.0;
        this.lat = 0.0;
        this.speed = 0.0;
        this.accuracy = 0.0;
        this.bearing = 0.0;
        this.timestamp = Time.getCurrentThreadTime();

    }

    public Cell(int cellId, int locationAreaCode, int mobileCountryCode, int mobileNetworkCode, int dbm, double accuracy, double speed,
                double bearing, int netType, long timestamp) {
        this.cellId = cellId;
        this.locationAreaCode = locationAreaCode;
        this.mobileCountryCode = mobileCountryCode;
        this.mobileNetworkCode = mobileNetworkCode;
        this.dbm = dbm;
        this.rssi = Integer.MAX_VALUE;
        this.timingAdvance = Integer.MAX_VALUE;
        this.sid = Integer.MAX_VALUE;
        this.accuracy = accuracy;
        this.speed = speed;
        this.bearing = bearing;
        this.netType = netType;
        this.timestamp = timestamp;
    }

    /**
     * Set Primary Scrambling Code (PSC) of current Cell
     *
     * @param primaryScramblingCode Primary Scrambling Code
     */
    public void setPrimaryScramblingCode(int primaryScramblingCode) {
        if (primaryScramblingCode == -1) {
            this.primaryScramblingCode = Integer.MAX_VALUE;
        } else {
            this.primaryScramblingCode = primaryScramblingCode;
        }
    }

    /**
     * Radio Access Technology (RAT)
     * <p>
     * Some places in the app refers to this as the Network Type.
     * <p>
     * For our purposes, network types displayed to the user is referred to as RAT.
     *
     * @return Current cell's Radio Access Technology (e.g. UMTS, GSM) or null if not known
     */
    public String getRat() {
        return getRatFromInt(this.netType);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + cellId;
        result = prime * result + locationAreaCode;
        result = prime * result + mobileCountryCode;
        result = prime * result + mobileNetworkCode;
        if (primaryScramblingCode != -1) {
            result = prime * result + primaryScramblingCode;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        Cell other = (Cell) obj;
        if (this.primaryScramblingCode != Integer.MAX_VALUE) {
            return this.cellId == other.getCellId() && this.locationAreaCode == other.getLocationAreaCode() && this.mobileCountryCode == other
                    .getMobileCountryCode() && this.mobileNetworkCode == other.getMobileNetworkCode() && this.primaryScramblingCode == other.getPrimaryScramblingCode();
        } else {
            return this.cellId == other.getCellId() && this.locationAreaCode == other.getLocationAreaCode() && this.mobileCountryCode == other
                    .getMobileCountryCode() && this.mobileNetworkCode == other.getMobileNetworkCode();
        }
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("cid - ").append(cellId).append("\n");
        result.append("LAC - ").append(locationAreaCode).append("\n");
        result.append("MCC - ").append(mobileCountryCode).append("\n");
        result.append("MNC - ").append(mobileNetworkCode).append("\n");
        result.append("DBm - ").append(dbm).append("\n");
        result.append("PSC - ").append(validatePscValue(primaryScramblingCode)).append("\n");
        result.append("Type - ").append(netType).append("\n");
        result.append("Lon - ").append(lon).append("\n");
        result.append("Lat - ").append(lat).append("\n");

        return result.toString();
    }

    public boolean isValid() {
        return this.getCellId() != Integer.MAX_VALUE && this.getLocationAreaCode() != Integer.MAX_VALUE;
    }

    /**
     * Get a human-readable string of RAT/Network Type
     * <p>
     * Frustratingly it looks like the app uses RAT & Network Type interchangably with both either
     * being an integer representation (TelephonyManager's constants) or a human-readable string.
     *
     * @param netType The integer representation of the network type, via TelephonyManager
     * @return Human-readable representation of network type (e.g. "EDGE", "LTE")
     */
    public static String getRatFromInt(int netType) {
        switch (netType) {
            case TelephonyConstants.NETWORK_SELECTION_UNKNOWN:
                return "HSDPA";
            case TelephonyConstants.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyConstants.NETWORK_TYPE_GSM:
                return "GSM";
            case TelephonyConstants.NETWORK_TYPE_NR:
                return "NR";
            case TelephonyConstants.NETWORK_TYPE_TDSCDMA:
                return "TDSCDMA";
            case TelephonyConstants.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyConstants.NETWORK_TYPE_WCDMA:
                return "WCDMA";
            case TelephonyConstants.NETWORK_TYPE_UNKNOWN:
                return "Unknown";
            default:
                return String.valueOf(netType);
        }
    }

    public static String validatePscValue(Context c, String psc) {
        return validatePscValue(c, Integer.parseInt(psc));
    }

    /**
     * Validate PSC is in bounds, return i18n'd "Unknown" if invalid
     *
     * @param c   Used for getString translations
     * @param psc
     * @return PSC or "Unknown "if invalid
     * @see #validatePscValue(int)
     */
    public static String validatePscValue(Context c, int psc) {
        String s = validatePscValue(psc);
        if (s.equals(INVALID_PSC)) {
            return c.getString(ResourceTable.String_unknown);
        }
        return s;
    }

    public static String validatePscValue(String psc) {
        return validatePscValue(Integer.parseInt(psc));
    }

    /**
     * Validate PSC is in bounds
     * <p>
     * Database import stores cell's PSC as "666" if its absent in OCID. This method will return
     * "invalid" instead.
     * <p>
     * Use this method to translate/i18n a cell's missing PSC value.
     *
     * @param psc
     * @return PSC or "invalid" untranslated string if invalid
     */
    public static String validatePscValue(int psc) {
        if (psc < 0 || psc > 511) {
            return INVALID_PSC;
        }
        return String.valueOf(psc);
    }

    // Parcelling
    public Cell(Parcel in) {
        String[] data = new String[15];

        in.readStringArray(data);
        cellId = Integer.parseInt(data[0]);
        locationAreaCode = Integer.parseInt(data[1]);
        mobileCountryCode = Integer.parseInt(data[2]);
        mobileNetworkCode = Integer.parseInt(data[3]);
        dbm = Integer.parseInt(data[4]);
        primaryScramblingCode = Integer.parseInt(data[5]);
        rssi = Integer.parseInt(data[6]);
        timingAdvance = Integer.parseInt(data[7]);
        sid = Integer.parseInt(data[8]);
        netType = Integer.parseInt(data[9]);
        lon = Double.parseDouble(data[10]);
        lat = Double.parseDouble(data[11]);
        speed = Double.parseDouble(data[12]);
        accuracy = Double.parseDouble(data[13]);
        bearing = Double.parseDouble(data[14]);
    }

    public int describeContents() {
        return 0;
    }


    @Override
    public boolean hasFileDescriptor() {
        return false;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeStringArray(new String[]{
                String.valueOf(this.cellId),
                String.valueOf(this.locationAreaCode),
                String.valueOf(this.mobileCountryCode),
                String.valueOf(this.mobileNetworkCode),
                String.valueOf(this.dbm),
                String.valueOf(this.primaryScramblingCode),
                String.valueOf(this.rssi),
                String.valueOf(this.timingAdvance),
                String.valueOf(this.sid),
                String.valueOf(this.netType),
                String.valueOf(this.lon),
                String.valueOf(this.lat),
                String.valueOf(this.speed),
                String.valueOf(this.accuracy),
                String.valueOf(this.bearing)});
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
