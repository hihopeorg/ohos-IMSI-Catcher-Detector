/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * desc   :日志封装
 */
public class LogUtils {
    /**
     * 类名
     */
    static String className;

    /**
     * 方法名
     */
    static String methodName;

    /**
     * 行数
     */
    static int lineNumber;

    static int CLASS_DOMAIN = 0x00201;
    static int CLASS_TYPE = HiLog.LOG_APP;
    private static HiLogLabel hiLogLabel;

    /**
     * type：用于指定输出日志的类型。HiLog中当前只提供了一种日志类型，即应用日志类型LOG_APP。
     * domain：用于指定输出日志所对应的业务领域，取值范围为0x0~0xFFFFF，开发者可以根据需要进行自定义。
     * tag：用于指定日志标识，可以为任意字符串，建议标识调用所在的类或者业务行为。
     */
    private static String createLog(String log) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("(")
                .append(methodName)
                .append(":" + lineNumber)
                .append(")");
        buffer.append(log);
        return buffer.toString();
    }

    /**
     * 获取文件名、方法名、所在行数
     */
    private static void getMethodNames(StackTraceElement[] sElements) {
        className = sElements[1].getFileName();
        methodName = sElements[1].getMethodName();
        lineNumber = sElements[1].getLineNumber();
        hiLogLabel = new HiLogLabel(CLASS_TYPE, CLASS_DOMAIN, className);
    }

    public static void debug(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.debug(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void info(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.info(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void error(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.error(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void fatal(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.fatal(hiLogLabel, " %{public}s", createLog(message));
    }

    public static void warn(String message) {
        getMethodNames(new Throwable().getStackTrace());
        HiLog.warn(hiLogLabel, " %{public}s", createLog(message));
    }
}
