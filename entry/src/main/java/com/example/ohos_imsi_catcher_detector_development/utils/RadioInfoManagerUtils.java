/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.utils;

import ohos.app.Context;
import ohos.telephony.NetworkState;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.TelephonyConstants;

public class RadioInfoManagerUtils {
    //卡1 : 0 ；卡2 :1 卡槽索引号，取值范围为0〜设备支持的最大卡槽索引号
    public static final int slotld = 0;
    private Context context;

    private RadioInfoManager radioInfoManager;

    public RadioInfoManagerUtils(Context context) {
        this.context = context;
        radioInfoManager = RadioInfoManager.getInstance(context);

    }

    public String getNetworkState(int slotld) {
        String netState = "unKnown";
        NetworkState networkState = radioInfoManager.getNetworkState(slotld);
        int networkStateRegState = networkState.getRegState();
        switch (networkStateRegState) {
            case NetworkState.REG_STATE_NO_SERVICE:
                netState = "Disconnected";
                break;
            case NetworkState.REG_STATE_IN_SERVICE:
                netState = "Connecting";
                break;
            case NetworkState.REG_STATE_EMERGENCY_CALL_ONLY:
                netState = "Indicates a state in which a device can use only the emergency call service.";
                break;
            case NetworkState.REG_STATE_POWER_OFF:
                netState = "Indicates that the cellular radio is powered off.";
                break;
        }
        return netState;
    }

    /**
     * get device of type
     */
    public String getPhoneType(int slotld) {
        /**根据需要添加 */
/*        TelephonyConstants#RADIO_TECHNOLOGY_1XRTT
        TelephonyConstants#RADIO_TECHNOLOGY_WCDMA
        TelephonyConstants#RADIO_TECHNOLOGY_HSPA
        TelephonyConstants#RADIO_TECHNOLOGY_HSPAP
        TelephonyConstants#RADIO_TECHNOLOGY_TD_SCDMA
        TelephonyConstants#RADIO_TECHNOLOGY_EVDO
        TelephonyConstants#RADIO_TECHNOLOGY_EHRPD
        TelephonyConstants#RADIO_TECHNOLOGY_LTE
        TelephonyConstants#RADIO_TECHNOLOGY_LTE_CA
        TelephonyConstants#RADIO_TECHNOLOGY_IWLAN*/
        String phoneTypeStr = "未知";
        int networkSelectionMode = radioInfoManager.getNetworkSelectionMode(slotld);
        switch (networkSelectionMode) {
            case TelephonyConstants.RADIO_TECHNOLOGY_GSM:
                phoneTypeStr = "GSM";
                break;
            case TelephonyConstants.NETWORK_TYPE_CDMA:
                phoneTypeStr = "CDMA";
                break;
            case TelephonyConstants.NETWORK_TYPE_NR:
                phoneTypeStr = "5G";
                break;
            case TelephonyConstants.RADIO_TECHNOLOGY_UNKNOWN:
                phoneTypeStr = "UNKNOWN";
                break;
        }
        return phoneTypeStr;
    }
}
