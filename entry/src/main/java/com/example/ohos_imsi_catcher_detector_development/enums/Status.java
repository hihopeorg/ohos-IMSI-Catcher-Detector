package com.example.ohos_imsi_catcher_detector_development.enums;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;

public enum Status {
    /**
     * Grey
     */
    IDLE(ResourceTable.String_status_idle, ResourceTable.Color_material_grey_400),
    /**
     * Green
     */
    OK(ResourceTable.String_status_ok, ResourceTable.Color_material_light_green_A700),
    /**
     * Yellow
     */
    MEDIUM(ResourceTable.String_status_medium, ResourceTable.Color_material_yellow_A700),
    /**
     * Orange
     */
    HIGH(ResourceTable.String_status_high, ResourceTable.Color_material_orange_A700),
    /**
     * Red
     */
    DANGER(ResourceTable.String_status_danger, ResourceTable.Color_material_red_A700),
    /**
     * Black
     */
    SKULL(ResourceTable.String_status_skull, ResourceTable.Color_material_black);

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    private int name;

    private int color;

    Status(int name, int color) {
        this.name = name;
        this.color = color;
    }
}
