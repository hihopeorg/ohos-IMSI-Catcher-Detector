package com.example.ohos_imsi_catcher_detector_development.enums;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum StatesDbViewer {

    UNIQUE_BTS_DATA(ResourceTable.String_unique_bts_data),
    BTS_MEASUREMENTS(ResourceTable.String_bts_measurements),
    IMPORTED_OCID_DATA(ResourceTable.String_imported_ocid_data),
    DEFAULT_MCC_LOCATIONS(ResourceTable.String_default_mmc_locations),
    SILENT_SMS(ResourceTable.String_silent_sms),
    MEASURED_SIGNAL_STRENGTHS(ResourceTable.String_measured_signal_strengths),
    EVENT_LOG(ResourceTable.String_eventlog),
    DETECTION_STRINGS(ResourceTable.String_detection_strings);
    //TODO DetectionFlags
    // DETECTION_FLAGS(R.string.detection_flags)

    private final int mStatementValue;

    StatesDbViewer(int pStatementValue) {
        mStatementValue = pStatementValue;
    }

    public int getStatementValue() {
        return mStatementValue;
    }


    public static List<StatesDbViewer> getStates() {
        return new ArrayList<>(Arrays.asList(values()));
    }

    public static StatesDbViewer getValueByOrdinal(int pOrdinal) {
        StatesDbViewer lResult = null;
        for (StatesDbViewer item : values()) {
            if (item.ordinal() == pOrdinal) {
                lResult = item;
                break;
            }
        }
        return lResult;
    }

    public String getDisplayName(Context pContext) {
        if (pContext == null) {
            return null;
        }
        return pContext.getString(getStatementValue());
    }

}
