package com.example.ohos_imsi_catcher_detector_development;

import com.example.ohos_imsi_catcher_detector_development.slice.ListItemSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ListItem extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ListItemSlice.class.getName());
    }
}
