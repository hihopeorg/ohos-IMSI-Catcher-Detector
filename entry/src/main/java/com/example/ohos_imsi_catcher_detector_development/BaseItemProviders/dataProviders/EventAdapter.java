package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.Event;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.DateFormat;


import static java.lang.String.valueOf;

public class EventAdapter extends RealmItemProvider<Event> {

    public EventAdapter(RealmResults<Event> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_eventlog_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Event event = getItem(position);
        holder.updateDisplay(event, position);

        return convertView;
    }


    private class ViewHolder {

        private final Component mRootView;
        private final Text mtime;
        private final Text mLAC;
        private final Text mCID;
        private final Text mPSC;
        private final Text mgpsd_lat;
        private final Text mgpsd_lon;
        private final Text mgpsd_accu;
        private final Text mDF_id;
        private final Text mDF_desc;

        private final Text mRecordId;

        ViewHolder(Component rootView) {
            mRootView = rootView;

            mtime =         (Text) mRootView.findComponentById(ResourceTable.Id_time);
            mLAC =          (Text) mRootView.findComponentById(ResourceTable.Id_LAC);
            mCID =          (Text) mRootView.findComponentById(ResourceTable.Id_CID);
            mPSC =          (Text) mRootView.findComponentById(ResourceTable.Id_PSC);
            mgpsd_lat =     (Text) mRootView.findComponentById(ResourceTable.Id_gpsd_lat);
            mgpsd_lon =     (Text) mRootView.findComponentById(ResourceTable.Id_gpsd_lon);
            mgpsd_accu =    (Text) mRootView.findComponentById(ResourceTable.Id_gpsd_accu);
            mDF_id =        (Text) mRootView.findComponentById(ResourceTable.Id_DF_id);
            mDF_desc =      (Text) mRootView.findComponentById(ResourceTable.Id_DF_desc);

            mRecordId =     (Text) mRootView.findComponentById(ResourceTable.Id_record_id);

            rootView.setTag(this);
        }

        public void updateDisplay(Event event, int position) {
            mtime.setText(DateFormat.getDateTimeInstance().format(event.getTimestamp()));          // need fix ?
            mLAC.setText(valueOf(event.getLocationAreaCode()));
            mCID.setText(valueOf(event.getCellId()));
            mPSC.setText(valueOf(event.getPrimaryScramblingCode()));
            mgpsd_lat.setText(valueOf(event.getGpsLocation().getLatitude()));
            mgpsd_lon.setText(valueOf(event.getGpsLocation().getLongitude()));
            mgpsd_accu.setText(valueOf(event.getGpsLocation().getAccuracy()));
            mDF_id.setText(valueOf(event.getDfId()));
            mDF_desc.setText(event.getDfDescription());

            mRecordId.setText(valueOf(position));
        }
    }
}
