/* ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;


import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.Measure;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.DateFormat;

import static java.lang.String.valueOf;

/**
 * Inflater class used in DB viewer (for Measured cell strength measurements)
 *
 * @author Tor Henning Ueland
 */
public class MeasuredCellStrengthAdapter extends RealmItemProvider<Measure> {

    public MeasuredCellStrengthAdapter(RealmResults<Measure> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_measured_signal_str, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Measure item = getItem(position);
        holder.updateDisplay(item);

        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;

        private final Text cid;
        private final Text rss;
        private final Text time;

        ViewHolder(Component rootView) {
            mRootView = rootView;

            cid = (Text) mRootView.findComponentById(ResourceTable.Id_tv_measure_cid);
            rss = (Text) mRootView.findComponentById(ResourceTable.Id_tv_measure_rss);
            time = (Text) mRootView.findComponentById(ResourceTable.Id_tv_measure_time);

            rootView.setTag(this);
        }

        public void updateDisplay(Measure item) {
            cid.setText(valueOf(item.getBaseStation().getCellId()));
            rss.setText(valueOf(item.getRxSignal()));
            time.setText(DateFormat.getDateTimeInstance().format(item.getTime()));
        }
    }
}
