/* Ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */

/* Coded by Paul Kinsella <paulkinsella29@yahoo.ie> */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;


import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.SmsData;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.util.Date;

import static java.lang.String.valueOf;


public class SmsDataAdapter extends RealmItemProvider<SmsData> {

    public SmsDataAdapter(RealmResults<SmsData> realmResults) {
        super(realmResults);
    }

    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_adv_user_sms_listview, parent, false);
            holder = new ViewHolder();
            holder.smsd_timestamp = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_timestamp);
            holder.smsd_smstype = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_smstype);
            holder.smsd_number = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_number);
            holder.smsd_data = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_msg);
            holder.smsd_lac = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_lac);
            holder.smsd_cid = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_cid);
            holder.smsd_rat = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_nettype);
            holder.smsd_roam = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_roaming);
            holder.smsd_lat = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_lat);
            holder.smsd_lon = (Text) convertView.findComponentById(ResourceTable.Id_tv_adv_smsdata_lon);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Date timestamp = getItem(position).getTimestamp();

        holder.smsd_timestamp.setText(java.text.DateFormat.getDateTimeInstance().format(timestamp));
        holder.smsd_smstype.setText(getItem(position).getType());
        holder.smsd_number.setText(getItem(position).getSenderNumber());
        holder.smsd_data.setText(getItem(position).getMessage());
        holder.smsd_lac.setText(valueOf(getItem(position).getLocationAreaCode()));
        holder.smsd_cid.setText(valueOf(getItem(position).getCellId()));
        holder.smsd_rat.setText(getItem(position).getRadioAccessTechnology());
        String isRoaming = "false";
        if (getItem(position).isRoaming()) {
            isRoaming = "true";
        }
        holder.smsd_roam.setText(isRoaming);
        holder.smsd_lat.setText(valueOf(getItem(position).getGpsLocation().getLatitude()));
        holder.smsd_lon.setText(valueOf(getItem(position).getGpsLocation().getLongitude()));

        return convertView;
    }

    static class ViewHolder {

        Text smsd_timestamp, smsd_smstype, smsd_number, smsd_data,
                smsd_lac, smsd_cid, smsd_rat, smsd_roam, smsd_lat, smsd_lon;
    }
}
