/* Ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.Import;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.DateFormat;

import static java.lang.String.valueOf;

public class ImportAdapter extends RealmItemProvider<Import> {

    public ImportAdapter(RealmResults<Import> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_dbe_import_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Import anImport = getItem(position);
        holder.updateDisplay(anImport, position);
        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;

        private final Text DB_SOURCE;
        private final Text RAT;
        private final Text MCC;
        private final Text MNC;
        private final Text LAC;
        private final Text CID;
        private final Text PSC;
        private final Text GPS_LAT;
        private final Text GPS_LON;
        private final Text IS_GPS_EXACT;
        private final Text AVG_RANGE;
        private final Text AVG_SIGNAL;
        private final Text SAMPLES;
        private final Text TIME_FIRST;
        private final Text TIME_LAST;
        private final Text REJ_CAUSE;

        private final Text mRecordId;

        ViewHolder(Component rootView) {
            mRootView = rootView;
            // TODO: explain these and try to adhere to a naming convention
            // These are the id names as used in the "dbe_import_items.xml" stylesheet
            DB_SOURCE =     (Text) mRootView.findComponentById(ResourceTable.Id_dbsource);
            RAT =           (Text) mRootView.findComponentById(ResourceTable.Id_RAT);
            MCC =           (Text) mRootView.findComponentById(ResourceTable.Id_nMCC);
            MNC =           (Text) mRootView.findComponentById(ResourceTable.Id_nMNC);
            LAC =           (Text) mRootView.findComponentById(ResourceTable.Id_nLAC);
            CID =           (Text) mRootView.findComponentById(ResourceTable.Id_nCID);
            PSC =           (Text) mRootView.findComponentById(ResourceTable.Id_nPSC);
            GPS_LAT =       (Text) mRootView.findComponentById(ResourceTable.Id_ngpsd_lat);
            GPS_LON =       (Text) mRootView.findComponentById(ResourceTable.Id_ngpsd_lon);
            IS_GPS_EXACT =  (Text) mRootView.findComponentById(ResourceTable.Id_is_exact);
            AVG_RANGE =     (Text) mRootView.findComponentById(ResourceTable.Id_navg_range);
            AVG_SIGNAL =    (Text) mRootView.findComponentById(ResourceTable.Id_navg_signal);
            SAMPLES =       (Text) mRootView.findComponentById(ResourceTable.Id_nSAMPLES);
            TIME_FIRST =    (Text) mRootView.findComponentById(ResourceTable.Id_nTIME_FIRST);
            TIME_LAST =     (Text) mRootView.findComponentById(ResourceTable.Id_nTIME_LAST);
            REJ_CAUSE =     (Text) mRootView.findComponentById(ResourceTable.Id_nREJ_CAUSE);

            mRecordId =     (Text) mRootView.findComponentById(ResourceTable.Id_record_id);
            rootView.setTag(this);
        }

        public void updateDisplay(Import anImport, int pos) {
            DB_SOURCE.setText(anImport.getDbSource());
            RAT.setText(anImport.getRadioAccessTechnology());
            MCC.setText(valueOf(anImport.getMobileCountryCode()));
            MNC.setText(valueOf(anImport.getMobileNetworkCode()));
            LAC.setText(valueOf(anImport.getLocationAreaCode()));
            CID.setText(valueOf(anImport.getCellId()));
            PSC.setText(valueOf(anImport.getPrimaryScramblingCode()));
            GPS_LAT.setText(valueOf(anImport.getGpsLocation().getLatitude()));
            GPS_LON.setText(valueOf(anImport.getGpsLocation().getLongitude()));
            IS_GPS_EXACT.setText(valueOf(anImport.isGpsExact()));
            AVG_RANGE.setText(valueOf(anImport.getAvgRange()));
            AVG_SIGNAL.setText(valueOf(anImport.getAvgSignal()));
            SAMPLES.setText(valueOf(anImport.getSamples()));

            DateFormat df = DateFormat.getDateTimeInstance();
            TIME_FIRST.setText(df.format(anImport.getTimeFirst()));
            TIME_LAST.setText(df.format(anImport.getTimeLast()));

            REJ_CAUSE.setText(valueOf(anImport.getRejCause()));

            mRecordId.setText(valueOf(pos));
        }
    }
}
