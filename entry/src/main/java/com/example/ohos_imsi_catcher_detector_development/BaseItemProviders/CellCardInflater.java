package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

public class CellCardInflater implements IAdapterComponentInflater<CardItemData> {
    @Override
    public Component inflate(final BaseInflaterAdapter<CardItemData> adapter,
                             final int pos, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_cell_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final CardItemData item = adapter.getTItem(pos);
        holder.updateDisplay(item);

        return convertView;
    }

    private class ViewHolder {

        // OLD (in old DB tables)
        private final Component mRootView;
        private final Text mCellID;
        private final Text mPsc;
        private final Text mLac;
        private final Text mMcc;
        private final Text mMnc;
        private final Text mNet;
        private final Text mLat;
        private final Text mLng;
        private final Text mSignal;
        private final Text mRecordId;

        // OLD (in old DB tables)
        ViewHolder(Component rootView) {
            mRootView = rootView;
            mCellID = (Text) mRootView.findComponentById(ResourceTable.Id_cellID);
            mCellID.setVisibility(Component.VERTICAL);
            mPsc = (Text) mRootView.findComponentById(ResourceTable.Id_psc);
            mPsc.setVisibility(Component.VERTICAL);
            mLac = (Text) mRootView.findComponentById(ResourceTable.Id_lac);
            mLac.setVisibility(Component.VERTICAL);
            mMcc = (Text) mRootView.findComponentById(ResourceTable.Id_mcc);
            mMcc.setVisibility(Component.VERTICAL);
            mMnc = (Text) mRootView.findComponentById(ResourceTable.Id_mnc);
            mMnc.setVisibility(Component.VERTICAL);
            mNet = (Text) mRootView.findComponentById(ResourceTable.Id_net);
            mNet.setVisibility(Component.VERTICAL);
            mLat = (Text) mRootView.findComponentById(ResourceTable.Id_lat);
            mLat.setVisibility(Component.VERTICAL);
            mLng = (Text) mRootView.findComponentById(ResourceTable.Id_lng);
            mLng.setVisibility(Component.VERTICAL);
            mSignal = (Text) mRootView.findComponentById(ResourceTable.Id_signal);
            mSignal.setVisibility(Component.VERTICAL);
            mRecordId = (Text) mRootView.findComponentById(ResourceTable.Id_record_id);


            rootView.setTag(this);
        }

        public void updateDisplay(CardItemData item) {
            if (!item.getCellId().contains("N/A")) {
                mCellID.setVisibility(Component.VISIBLE);
                mCellID.setText(item.getCellId());
            }

            if (!item.getPsc().contains("N/A")) {
                mPsc.setVisibility(Component.VISIBLE);
                mPsc.setText(item.getPsc());
            }

            if (!item.getLac().contains("N/A")) {
                mLac.setVisibility(Component.VISIBLE);
                mLac.setText(item.getLac());
            }

            if (!item.getNet().contains("N/A")) {
                mNet.setVisibility(Component.VISIBLE);
                mNet.setText(item.getNet());
            }

            if (!item.getLat().contains("N/A")) {
                mLat.setVisibility(Component.VISIBLE);
                mLat.setText(item.getLat());
            }

            if (!item.getLon().contains("N/A")) {
                mLng.setVisibility(Component.VISIBLE);
                mLng.setText(item.getLon());
            }

            if (!item.getMcc().contains("N/A")) {
                mMcc.setVisibility(Component.VISIBLE);
                mMcc.setText(item.getMcc());
            }

            if (!item.getMnc().contains("N/A")) {
                mMnc.setVisibility(Component.VISIBLE);
                mMnc.setText(item.getMnc());
            }

            if (!item.getSignal().contains("N/A")) {
                mSignal.setVisibility(Component.VISIBLE);
                mSignal.setText(item.getSignal());
            }

            mRecordId.setText(item.getRecordId());
        }
    }
}
