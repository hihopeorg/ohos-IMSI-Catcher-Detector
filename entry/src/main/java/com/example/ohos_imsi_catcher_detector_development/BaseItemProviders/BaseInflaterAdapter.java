/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.ArrayList;
import java.util.List;

public class BaseInflaterAdapter<T> extends BaseItemProvider {
    private final List<T> m_items = new ArrayList<>();

    private IAdapterComponentInflater<T> m_viewInflater;

    public BaseInflaterAdapter(IAdapterComponentInflater<T> viewInflater) {
        m_viewInflater = viewInflater;
    }

    public BaseInflaterAdapter(List<T> items, IAdapterComponentInflater<T> viewInflater) {
        m_items.addAll(items);
        m_viewInflater = viewInflater;
    }

    public void setViewInflater(IAdapterComponentInflater<T> viewInflater, boolean notifyChange) {
        m_viewInflater = viewInflater;

        if (notifyChange) {
            notifyDataChanged();
        }
    }

    public void addItem(T item, boolean notifyChange) {
        m_items.add(item);

        if (notifyChange) {
            notifyDataChanged();
        }
    }

    public void addItems(List<T> items, boolean notifyChange) {
        m_items.addAll(items);

        if (notifyChange) {
            notifyDataChanged();
        }
    }

    public void clear(boolean notifyChange) {
        m_items.clear();

        if (notifyChange) {
            notifyDataChanged();
        }
    }

    @Override
    public int getCount() {
        return m_items.size();
    }

    @Override
    public Object getItem(int pos) {
        return getItemId(pos);
    }

    public T getTItem(int pos) {
        return m_items.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public Component getComponent(int pos, Component convertView, ComponentContainer parent) {
        return m_viewInflater != null ? m_viewInflater.inflate(this, pos, convertView, parent)
                : null;
    }
}
