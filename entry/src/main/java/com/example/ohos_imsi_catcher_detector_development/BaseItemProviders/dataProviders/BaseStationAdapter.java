/* Ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.BaseTransceiverStation;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.DateFormat;

import static java.lang.String.valueOf;

/**
 * Contains the data and definitions of all the items of the XML layout
 * <p/>
 * TODO: Add DB items: T3212, A5x and ST_id
 */
public class BaseStationAdapter extends RealmItemProvider<BaseTransceiverStation> {

    public BaseStationAdapter(RealmResults<BaseTransceiverStation> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_unique_bts_data, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final BaseTransceiverStation item = getItem(position);
        holder.updateDisplay(item, position);

        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;

        private final Text LAC;
        private final Text CID;
        private final Text MCC;
        private final Text MNC;
        private final Text PSC;

        private final Text T3212;
        private final Text A5X;
        private final Text ST_ID;

        private final Text TIME_FIRST;
        private final Text TIME_LAST;
        private final Text LAT;
        private final Text LON;

        private final Text RecordId;


        // These are the names of the "@+id/xxxx" items in the XML layout file
        ViewHolder(Component rootView) {
            mRootView = rootView;

            LAC = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_lac);
            CID = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_cid);
            MCC = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_mcc);
            MNC = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_mnc);
            PSC = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_psc);
            T3212 = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_t3212);
            A5X = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_a5x);
            ST_ID = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_st_id);
            TIME_FIRST = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_time_first);
            TIME_LAST = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_time_last);
            LAT = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_lat);
            LON = (Text) mRootView.findComponentById(ResourceTable.Id_tv_uniquebts_lon);

            RecordId = (Text) mRootView.findComponentById(ResourceTable.Id_record_id);
            rootView.setTag(this);
        }

        public void updateDisplay(BaseTransceiverStation baseStation, int position) {

            LAC.setText(valueOf(baseStation.getLocationAreaCode()));
            CID.setText(valueOf(baseStation.getCellId()));

            MCC.setText(valueOf(baseStation.getMobileCountryCode()));
            MNC.setText(valueOf(baseStation.getMobileNetworkCode()));
            PSC.setText(valueOf(baseStation.getPrimaryScramblingCode()));

            DateFormat dateFormat = DateFormat.getDateTimeInstance();
            TIME_FIRST.setText(dateFormat.format(baseStation.getTimeFirst()));
            TIME_LAST.setText(dateFormat.format(baseStation.getTimeLast()));

            LAT.setText(valueOf(baseStation.getGpsLocation().getLatitude()));
            LON.setText(valueOf(baseStation.getGpsLocation().getLongitude()));

            RecordId.setText(valueOf(position));
        }
    }
}
