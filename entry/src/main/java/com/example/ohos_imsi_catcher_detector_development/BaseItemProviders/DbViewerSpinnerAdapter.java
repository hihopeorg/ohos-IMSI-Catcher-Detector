package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.components.holders.ViewTableNameSpinnerHolder;
import com.example.ohos_imsi_catcher_detector_development.enums.StatesDbViewer;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

public class DbViewerSpinnerAdapter extends BaseItemProviderAdapter<StatesDbViewer> {

    private final List<StatesDbViewer> mDataList;

    public DbViewerSpinnerAdapter(Context context, int layout_item_spinner_db_viewer) {
        super(context, layout_item_spinner_db_viewer);

        mDataList = StatesDbViewer.getStates();
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Component getComponent(int pPosition, Component component, ComponentContainer componentContainer) {
        Component lView = component;
        ViewTableNameSpinnerHolder lHolder;
        if (lView == null
                || (lView.getId() != ResourceTable.Id_item_root_layout && !(lView.getTag() instanceof ViewTableNameSpinnerHolder))) {

            lView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_spinner_db_viewer,componentContainer, false);
            lHolder = setViewHolderToView(lView);

        } else {
            lHolder = (ViewTableNameSpinnerHolder) lView.getTag();

        }

        StatesDbViewer lEntry = mDataList.get(pPosition);
        lHolder.name.setText(lEntry.getDisplayName(mContext));

        return lView;
    }

    @Override
    public StatesDbViewer getItem(int position) {
        return mDataList.get(position);
    }

    private ViewTableNameSpinnerHolder setViewHolderToView(Component pView) {
        ViewTableNameSpinnerHolder lHolder;
        lHolder = new ViewTableNameSpinnerHolder();

        lHolder.name = (Text) pView.findComponentById(ResourceTable.Id_item_name);

        pView.setTag(lHolder);
        return lHolder;
    }

    @Override
    public void convert(ViewHolder holder, StatesDbViewer statesDbViewer) {

    }
}
