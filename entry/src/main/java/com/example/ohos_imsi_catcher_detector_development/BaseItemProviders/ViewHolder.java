/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.HashMap;

public class ViewHolder {
    private HashMap<Integer, Component> mViews;
    private int mPosition;
    private Component mConvertView;
    private Context mContext;
    private int mLayoutId;

    public ViewHolder(Context context, ComponentContainer parent, int layoutId, int position) {
        mContext = context;
        mLayoutId = layoutId;
        this.mPosition = position;
        this.mViews = new HashMap<>();
        mConvertView = LayoutScatter.getInstance(mContext).parse(layoutId, null, false);

        mConvertView.setTag(this);
    }

    public static ViewHolder get(Context context, Component convertView,
                                 ComponentContainer parent, int layoutId, int position) {
        if (convertView == null) {
            return new ViewHolder(context, parent, layoutId, position);
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.mPosition = position;
            return holder;
        }
    }

    public int getPosition() {
        return mPosition;
    }

    public int getLayoutId() {
        return mLayoutId;
    }

    /**
     * 通过viewId获取控件
     *
     * @param viewId
     * @return
     */
    public <T extends Component> T getView(int viewId) {
        Component view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }


    public Component getConvertView() {
        return mConvertView;
    }

    /**
     * 设置TextView的文本
     *
     * @param viewId
     * @param text
     * @return
     */
    public ViewHolder setText(int viewId, String text) {
        Text tv = getView(viewId);
        tv.setText(text);
        return this;
    }

    public ViewHolder setVisible(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
        return this;
    }

    public ViewHolder setInvisible(int viewId, boolean invisible) {
        Component view = getView(viewId);
        view.setVisibility(invisible ? Component.VISIBLE : Component.INVISIBLE);
        return this;
    }

    /**
     * 关于事件
     */
    public ViewHolder setClickedListener(int viewId, Component.ClickedListener listener) {
        Component view = getView(viewId);
        view.setClickedListener(listener);
        return this;
    }

    public ViewHolder setTouchEventListener(int viewId, Component.TouchEventListener listener) {
        Component view = getView(viewId);
        view.setTouchEventListener(listener);
        return this;
    }

    public ViewHolder setLongClickedListener(int viewId, Component.LongClickedListener listener) {
        Component view = getView(viewId);
        view.setLongClickedListener(listener);
        return this;
    }

    public ViewHolder setDoubleClickedListener(int viewId, Component.DoubleClickedListener listener) {
        Component view = getView(viewId);
        view.setDoubleClickedListener(listener);
        return this;
    }

}
