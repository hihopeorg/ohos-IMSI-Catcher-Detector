package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.constants.DrawerMenu;
import com.example.ohos_imsi_catcher_detector_development.drawer.DrawerMenuItem;
import com.example.ohos_imsi_catcher_detector_development.drawer.DrawerMenuSection;
import com.example.ohos_imsi_catcher_detector_development.drawer.NavDrawerItem;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

public class DrawerMenuAdapter extends BaseItemProvider {

    private final LayoutScatter inflater;
    private final Context appContext;
    private final Component.ClickedListener clickedListener;
    private final List<NavDrawerItem> mMenu;
    HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "cyp");

    public DrawerMenuAdapter(Context context, int layout_drawer_item, List<NavDrawerItem> menu) {
        inflater = LayoutScatter.getInstance(context);
        appContext = context.getApplicationContext();
        this.mMenu = menu;
        clickedListener = new Component.ClickedListener() {
            @Override
            public void onClick(Component pView) {
                if (pView.getTag() != null && pView.getTag() instanceof Integer) {
                    showHelpToast((Integer) pView.getTag());
                }
            }
        };
    }

    private void showHelpToast(Integer pToastValueId) {
        new ToastDialog(appContext).setText(appContext.getString(pToastValueId)).show();
    }

    @Override
    public int getCount() {
        HiLog.info(label, "getCount：" + mMenu.size());
        return mMenu.size();
    }

    @Override
    public int getComponentTypeCount() {
        return DrawerMenu.COUNT_OF_MENU_TYPE;
    }

    @Override
    public int getItemComponentType(int position) {
        HiLog.info(label, "getItemComponentType==：" + mMenu.get(position).getType());
        return mMenu.get(position).getType();
    }

    @Override
    public Object getItem(int position) {
        HiLog.info(label, "getItem：" + position);
        return mMenu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        Component view = null;
        NavDrawerItem menuItem = mMenu.get(position);
        if (menuItem.getType() == DrawerMenuItem.ITEM_TYPE) {
            view = getItemView(convertView, parent, menuItem, position);
        } else {
//            view = getSectionView(convertView, parent, menuItem);
        }

        return view;
    }

    private Component getItemView(Component convertView, ComponentContainer parentView, NavDrawerItem navDrawerItem, int position) {
        DrawerMenuItem menuItem = (DrawerMenuItem) navDrawerItem;
        NavMenuItemHolder navMenuItemHolder = null;

        if (convertView == null) {
            convertView = inflater.parse(ResourceTable.Layout_drawer_item, parentView, false);
            Text labelView = (Text) convertView.findComponentById(ResourceTable.Id_drawer_menu_item_label);
            Image iconView = (Image) convertView.findComponentById(ResourceTable.Id_drawer_menu_item_icon);
            Image lInfoButton = (Image) convertView.findComponentById(ResourceTable.Id_drawer_menu_item_info_button);

            navMenuItemHolder = new NavMenuItemHolder();
            navMenuItemHolder.itemName = labelView;
            navMenuItemHolder.itemIcon = iconView;
            navMenuItemHolder.itemInfoButton = lInfoButton;

            convertView.setTag(navMenuItemHolder);
        }

        if (navMenuItemHolder == null) {
            navMenuItemHolder = (NavMenuItemHolder) convertView.getTag();
        }

        navMenuItemHolder.itemName.setText(menuItem.getLabel());
        navMenuItemHolder.itemIcon.setPixelMap(menuItem.getId());
        if (menuItem.isShowInfoButton()) {
            navMenuItemHolder.itemInfoButton.setTag(menuItem.getHelpStringId());
            navMenuItemHolder.itemInfoButton.setVisibility(Component.VISIBLE);
            navMenuItemHolder.itemInfoButton.setClickedListener(clickedListener);
        } else {
            navMenuItemHolder.itemInfoButton.setTag(menuItem.getHelpStringId());
            navMenuItemHolder.itemInfoButton.setVisibility(Component.INVISIBLE);
            navMenuItemHolder.itemInfoButton.setClickedListener(null);
        }
        convertView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.ohos_imsi_catcher_detector_development")
                        .withAbilityName("com.example.ohos_imsi_catcher_detector_development.Device")
                        .build();
                intent.setOperation(operation);
                component.getContext().startAbility(intent, position);
            }
        });
        return convertView;
    }

    private static class NavMenuItemHolder {
        Text itemName;
        Image itemIcon;
        Image itemInfoButton;
    }

    private class NavMenuSectionHolder {
        private Text itemName;
    }

    Component getSectionView(Component convertView, ComponentContainer parentView,
                             NavDrawerItem navDrawerItem) {

        DrawerMenuSection menuSection = (DrawerMenuSection) navDrawerItem;
        NavMenuSectionHolder navMenuItemHolder = null;

        if (convertView == null) {
            convertView = inflater.parse(ResourceTable.Layout_drawer_section, parentView, false);
            Text labelView = (Text) convertView.findComponentById(ResourceTable.Id_drawer_menu_section_label);

            navMenuItemHolder = new NavMenuSectionHolder();
            navMenuItemHolder.itemName = labelView;
            convertView.setTag(navMenuItemHolder);
        }

        if (navMenuItemHolder == null) {
            navMenuItemHolder = (NavMenuSectionHolder) convertView.getTag();
        }

        navMenuItemHolder.itemName.setText(menuSection.getLabel());

        return convertView;
    }
}
