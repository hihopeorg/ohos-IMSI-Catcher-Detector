package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.Measure;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.DateFormat;

import static java.lang.String.valueOf;

/**
 * Contains the data and definitions of all the items of the XML layout
 */
public class MeasureAdapter extends RealmItemProvider<Measure> {


    public MeasureAdapter(RealmResults<Measure> realmResults, int position) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_bts_measure_data, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Measure item = getItem(position);
        holder.updateDisplay(item, position);
        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;

        private Text bts_id;
        private Text nc_list;
        private Text time;
        private Text gpsd_lat;
        private Text gpsd_lon;
        private Text gpsd_accu;
        private Text rx_signal;
        private Text rat;
        private Text isSubmitted;
        private Text isNeighbor;

        private Text mRecordId;

        ViewHolder(Component rootView) {
            mRootView = rootView;

            bts_id = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_bts_id);
            nc_list = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_nc_list);
            time = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_time);
            gpsd_lat = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_gpsd_lat);
            gpsd_lon = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_gpsd_lon);
            gpsd_accu = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_gpsd_accu);

            rx_signal = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_rx_signal);
            rat = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_rat);

            isSubmitted = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_isSubmitted);
            isNeighbor = (Text) mRootView.findComponentById(ResourceTable.Id_tv_bts_measure_isNeighbor);

            mRecordId = (Text) mRootView.findComponentById(ResourceTable.Id_record_id);      // EVA
            rootView.setTag(this);
        }

        public void updateDisplay(Measure item, int position) {

            bts_id.setText(valueOf(item.getBaseStation().getCellId()));
            time.setText(DateFormat.getDateTimeInstance().format(item.getTime()));
            gpsd_lat.setText(valueOf(item.getGpsLocation().getLatitude()));
            gpsd_lon.setText(valueOf(item.getGpsLocation().getLongitude()));
            gpsd_accu.setText(valueOf(item.getGpsLocation().getAccuracy()));
            rx_signal.setText(valueOf(item.getRxSignal()));
            rat.setText(item.getRadioAccessTechnology());
            isSubmitted.setText(valueOf(item.isSubmitted()));
            isNeighbor.setText(valueOf(item.isNeighbor()));

            mRecordId.setText(valueOf(position));
        }
    }
}
