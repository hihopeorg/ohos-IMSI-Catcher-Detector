
/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;
import io.realm.*;
import ohos.agp.components.BaseItemProvider;

public abstract class RealmItemProvider<T extends RealmModel> extends BaseItemProvider {
    protected OrderedRealmCollection<T> adapterData;
    private final RealmChangeListener<OrderedRealmCollection<T>> listener;

    public RealmItemProvider(OrderedRealmCollection<T> data) {
        if (data != null && !data.isManaged()) {
            throw new IllegalStateException("Only use this adapter with managed list, for un-managed lists you can just use the BaseAdapter");
        } else {
            this.adapterData = data;
            this.listener = new RealmChangeListener<OrderedRealmCollection<T>>() {
                public void onChange(OrderedRealmCollection<T> results) {
                    RealmItemProvider.this.notifyDataChanged();
                }
            };
            if (data != null) {
                this.addListener(data);
            }

        }
    }

    private void addListener(OrderedRealmCollection<T> data) {
        if (data instanceof RealmResults) {
            RealmResults<T> results = (RealmResults) data;
            results.addChangeListener((OrderedRealmCollectionChangeListener<RealmResults<T>>) this.listener);
        } else {
            if (!(data instanceof RealmList)) {
                throw new IllegalArgumentException("RealmCollection not supported: " + data.getClass());
            }

            RealmList<T> list = (RealmList) data;
            list.addChangeListener((OrderedRealmCollectionChangeListener<RealmList<T>>) this.listener);
        }

    }

    private void removeListener(OrderedRealmCollection<T> data) {
        if (data instanceof RealmResults) {
            RealmResults<T> results = (RealmResults) data;
            results.removeChangeListener((OrderedRealmCollectionChangeListener<RealmResults<T>>) this.listener);
        } else {
            if (!(data instanceof RealmList)) {
                throw new IllegalArgumentException("RealmCollection not supported: " + data.getClass());
            }

            RealmList<T> list = (RealmList) data;
            list.removeChangeListener((OrderedRealmCollectionChangeListener<RealmList<T>>) this.listener);
        }

    }

    public int getCount() {
        return adapterData == null ? 0 : adapterData.size();
    }

    public T getItem(int position) {
        return adapterData == null ? null : (T) adapterData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void updateData(OrderedRealmCollection<T> data) {
        if (this.listener != null) {
            if (this.adapterData != null) {
                this.removeListener(this.adapterData);
            }

            if (data != null) {
                this.addListener(data);
            }
        }

        this.adapterData = data;
        this.notifyDataChanged();
    }
}
