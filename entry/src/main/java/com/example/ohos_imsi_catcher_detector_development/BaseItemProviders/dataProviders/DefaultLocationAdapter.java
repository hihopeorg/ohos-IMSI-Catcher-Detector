/* Ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;

import com.example.ohos_imsi_catcher_detector_development.mode.DefaultLocation;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import static java.lang.String.valueOf;

public class DefaultLocationAdapter extends RealmItemProvider<DefaultLocation> {

    public DefaultLocationAdapter(RealmResults<DefaultLocation> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_default_location_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DefaultLocation defaultLocation = getItem(position);
        holder.updateDisplay(defaultLocation, position);

        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;
        private final Text mCountry;
        private final Text mMcc;
        private final Text mLat;
        private final Text mLng;
        private final Text mRecordId;

        ViewHolder(Component rootView) {
            mRootView = rootView;

            mCountry =  (Text) mRootView.findComponentById(ResourceTable.Id_country);
            mMcc =      (Text) mRootView.findComponentById(ResourceTable.Id_mcc);
            mLat =      (Text) mRootView.findComponentById(ResourceTable.Id_lat);
            mLng =      (Text) mRootView.findComponentById(ResourceTable.Id_lng);
            mRecordId = (Text) mRootView.findComponentById(ResourceTable.Id_record_id);

            rootView.setTag(this);
        }

        public void updateDisplay(DefaultLocation defaultLocation, int position) {
            mCountry.setText(defaultLocation.getCountry());
            mMcc.setText(valueOf(defaultLocation.getMobileCountryCode()));
            mLat.setText(valueOf(defaultLocation.getGpsLocation().getLatitude()));
            mLng.setText(valueOf(defaultLocation.getGpsLocation().getLongitude()));
            mRecordId.setText(valueOf(position));
        }
    }
}
