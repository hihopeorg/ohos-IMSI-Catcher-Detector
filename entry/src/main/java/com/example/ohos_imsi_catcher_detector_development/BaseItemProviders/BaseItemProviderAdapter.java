/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public abstract class BaseItemProviderAdapter<T> extends BaseItemProvider {
    protected Context mContext;
    protected List<T> mDatas;
    protected LayoutScatter mInflater;
    private int layoutId;
    private int displayCount;

    public BaseItemProviderAdapter(Context context, List<T> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutScatter.getInstance(mContext);
    }

    public BaseItemProviderAdapter(Context context, List<T> datas, int layoutId) {
        this.mContext = context;
        this.mDatas = datas;
        this.layoutId = layoutId;
        mInflater = LayoutScatter.getInstance(mContext);
    }

    public BaseItemProviderAdapter(Context context, int layoutId) {
        this.mContext = context;
        this.layoutId = layoutId;
        mInflater = LayoutScatter.getInstance(mContext);
    }

    @Override
    public int getCount() {
        if (displayCount != 0) {
            return this.displayCount;
        }
        return mDatas.size();
    }

    public void setDisplayCount(int displayCount) {
        this.displayCount = displayCount;
    }

    @Override
    public T getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {

        ViewHolder holder = ViewHolder.get(mContext, convertView, parent,
                layoutId, position);
        convert(holder, getItem(position));
        return holder.getConvertView();
    }

    public List<T> getDatas() {
        return mDatas;
    }

    public void setDatas(List<T> datas) {
        mDatas = datas;
    }

    public abstract void convert(ViewHolder holder, T t);

}
