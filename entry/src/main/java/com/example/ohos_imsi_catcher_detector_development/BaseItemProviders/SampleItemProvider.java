/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

public class SampleItemProvider extends BaseItemProvider {
    private final List<SettingItem> settingList;
    private final AbilitySlice slice;

    public SampleItemProvider(List<SettingItem> list, AbilitySlice slice) {
        this.settingList = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return settingList == null ? 0 : settingList.size();
    }

    @Override
    public Object getItem(int position) {
        if (settingList != null && position > 0 && position < settingList.size()) {
            return settingList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        SettingHolder holder;
        SettingItem setting = settingList.get(position);
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_layout_item_setting, null, false);
            holder = new SettingHolder(cpt);
            //将获取到的子组件信息绑定到列表项的实例中
            cpt.setTag(holder);
        } else {
            cpt = component;
            //从缓存中获取到列表项实例后，直接使用绑定的子组件信息进行数据填充。
            holder = (SettingHolder) cpt.getTag();
        }
        holder.settingIma.setPixelMap(setting.getImageId());
        holder.settingText.setText(setting.getSettingName());
        holder.settingSwitch.setPixelMap(setting.isChecked());
        cpt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                String bundleName = "com.example.ohos_imsi_catcher_detector_development";
                String abilityName;
                switch (position) {
                    case 0:
                        abilityName = "com.example.ohos_imsi_catcher_detector_development.Device";
                        getToAbility(position, bundleName, abilityName, component.getContext());
                        break;
                    case 1:
                        new ToastDialog(component.getContext()).setText("HarmonyIMSICatcherDetector：刷新间隔时间15秒").show();
                        abilityName = "com.example.ohos_imsi_catcher_detector_development.CellInfo";
                        getToAbility(position, bundleName, abilityName, component.getContext());
                        break;
                    case 2:
                        abilityName = "com.example.ohos_imsi_catcher_detector_development.DbViewer";
                        getToAbility(position, bundleName, abilityName, component.getContext());
                        break;
                    case 3:
                        new ToastDialog(component.getContext()).setText("AIMSICD：暂不支持").show();
                        break;
                    case 4:
                        new ToastDialog(component.getContext()).setText("未查到找串行设备").show();
                        abilityName = "com.example.ohos_imsi_catcher_detector_development.AtCommand";
                        getToAbility(position, bundleName, abilityName, component.getContext());
                        break;
                }
            }
        });
        return cpt;
    }

    private void getToAbility(int position, String bundleName, String abilityName, Context context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundleName)
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, position);
    }

    //用于保存列表项中的子组件信息
    public class SettingHolder {
        Image settingIma;
        Text settingText;
        Image settingSwitch;

        SettingHolder(Component component) {
            settingIma = (Image) component.findComponentById(ResourceTable.Id_ima_setting);
            settingText = (Text) component.findComponentById(ResourceTable.Id_text_setting);
            settingSwitch = (Image) component.findComponentById(ResourceTable.Id_switch_setting);

        }

        private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
            StateElement trackElement = new StateElement();
            trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
            trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
            return trackElement;
        }


        private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
            StateElement thumbElement = new StateElement();
            thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
            thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
            return thumbElement;
        }
    }
}
