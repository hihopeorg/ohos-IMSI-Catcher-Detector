package com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders;


import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.mode.SmsDetectionString;
import io.realm.RealmResults;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

public class DetectionStringAdapter extends RealmItemProvider<SmsDetectionString> {

    public DetectionStringAdapter(RealmResults<SmsDetectionString> realmResults) {
        super(realmResults);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(parent.getContext());
            convertView = inflater.parse(ResourceTable.Layout_detection_strings_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SmsDetectionString smsDetectionString = getItem(position);
        holder.updateDisplay(smsDetectionString);

        return convertView;
    }

    private class ViewHolder {

        private final Component mRootView;
        private final Text d_string;
        private final Text d_type;


        ViewHolder(Component rootView) {
            mRootView = rootView;
            d_string = (Text) mRootView.findComponentById(ResourceTable.Id_tv_det_str_info);
            d_type = (Text) mRootView.findComponentById(ResourceTable.Id_tv_det_type_info);

            rootView.setTag(this);
        }

        public void updateDisplay(SmsDetectionString smsDetectionString) {
            d_string.setText(smsDetectionString.getDetectionString());
            d_type.setText(smsDetectionString.getSmsType());
        }
    }
}
