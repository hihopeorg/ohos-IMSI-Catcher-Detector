/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.SampleItemProvider;
import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.SettingItem;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class ListItemSlice extends AbilitySlice {

    private ListContainer listContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list_container);
        initListContainer();
    }

    private void initListContainer() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<SettingItem> list = getToAbility();
        list.get(0).setImageId(ResourceTable.Media_ic_action_phone);
        list.get(0).setSettingName("手机/SIM卡详细信息");
        list.get(1).setImageId(ResourceTable.Media_cell_tower);
        list.get(1).setSettingName("邻居基站");
        list.get(2).setImageId(ResourceTable.Media_ic_action_storage);
        list.get(2).setSettingName("数据库查看器");
        list.get(3).setImageId(ResourceTable.Media_ic_action_map);
        list.get(3).setSettingName("基站地图查看器");
        list.get(4).setImageId(ResourceTable.Media_ic_action_computer);
        list.get(4).setSettingName("AT命令接口");
        SampleItemProvider sampleItemProvider = new SampleItemProvider(list, this);
        listContainer.setItemProvider(sampleItemProvider);
    }

    private List<SettingItem> getToAbility() {
        ArrayList<SettingItem> data = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            data.add(new SettingItem(
                    ResourceTable.Media_icon,
                    "手机信息" + i,
                    ResourceTable.Media_ic_info
            ));
        }
        return data;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
