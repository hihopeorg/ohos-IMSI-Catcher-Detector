/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.DbViewerSpinnerAdapter;
import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.RealmItemProvider;
import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.dataProviders.*;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.enums.StatesDbViewer;
import com.example.ohos_imsi_catcher_detector_development.mode.*;
import com.example.ohos_imsi_catcher_detector_development.utils.RealmHelper;
import io.realm.Realm;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

public class DbViewerSlice extends AbilitySlice {

    private ListContainer list_db;
    private ListContainer list_tblSpinner;
    private StatesDbViewer mTableSelected;
    private Component emptyView;
    private Realm realm;
    private RealmHelper mDb;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_db_viewer);
        initListContainer();
//        realm = Realm.getDefaultInstance();
    }

    private void initListContainer() {
        list_tblSpinner = (ListContainer) findComponentById(ResourceTable.Id_table_spinner);
        list_db = (ListContainer) findComponentById(ResourceTable.Id_list_container_db);
        DbViewerSpinnerAdapter mSpinnerAdapter = new DbViewerSpinnerAdapter(getContext(), ResourceTable.Layout_item_spinner_db_viewer);
        list_tblSpinner.setItemProvider(mSpinnerAdapter);
        list_tblSpinner.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                Object selectedItem = list_tblSpinner.getItemProvider().getItem(position);
                if (!(selectedItem instanceof StatesDbViewer)) {
                    return;
                }
                mTableSelected = (StatesDbViewer) selectedItem;

                /*switch (position) {
                    case 0:
                        setListAdapter(new BaseStationAdapter(realm.where(BaseTransceiverStation.class).findAll()));
                        break;
                    case 1:
                        setListAdapter(new MeasureAdapter(realm.where(Measure.class).findAll(), position));
                        break;
                    case 2:
                        setListAdapter(new ImportAdapter(realm.where(Import.class).findAll()));
                        break;
                    case 3:
                        setListAdapter(new DefaultLocationAdapter(realm.where(DefaultLocation.class).findAll()));
                        break;
                    case 4:  //Silent SMS
                        setListAdapter(new SmsDataAdapter(realm.where(SmsData.class).findAll()));
                        break;
                    case 5:
                        setListAdapter(new MeasuredCellStrengthAdapter(realm.where(Measure.class).findAll()));
                        break;
                    case 6:
                        setListAdapter(new EventAdapter(realm.where(Event.class).findAll()));
                        break;
                    case 7:
                        setListAdapter(new DetectionStringAdapter(realm.where(SmsDetectionString.class).findAll()));
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown type of table");
                }*/
            }
        });

    }

    private void setListAdapter(RealmItemProvider adapter) {
        if (adapter != null) {
            list_db.setItemProvider(adapter);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        mDb = new RealmHelper(getAbility().getContext());
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (realm != null && !realm.isClosed()) {
            realm.close();
        }
    }
}
