/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.BaseInflaterAdapter;
import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.CardItemData;
import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.CellCardInflater;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.services.AimsicdService;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.bundle.ElementName;
import ohos.rpc.IRemoteObject;

public class CellInfoSlice extends AbilitySlice {

    private ListContainer listContainer;
    private BaseInflaterAdapter<CardItemData> mBaseInflaterAdapter;
    private CellInfoAdapter mCellInfoAdapter;
    private Intent serviceIntent;
    private boolean mBound;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_cell_info);
        initListContainer();

        startservices();
    }

    private void initListContainer() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container_cell);
        mBaseInflaterAdapter = new BaseInflaterAdapter<>(new CellCardInflater());
        mCellInfoAdapter = new CellInfoAdapter(mBaseInflaterAdapter, new CellInfoOverviewData());
        listContainer.setItemProvider(mCellInfoAdapter);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    class CellInfoOverviewData {
        String mNeighboringCells;
        int mNeighboringCellsVisibility = -1;

        int mNeighboringTotal;
        int mNeighboringTotalViewVisibility = -1;

        int mCipheringIndicatorLabelVisibility = -1;

        String mCipheringIndicator;
        int mCipheringIndicatorVisibility = -1;
    }

    class CellInfoOverviewHolder {
        Text mNeighboringCells;
        Text mNeighboringTotal;
        Text mCipheringIndicatorLabel;
        Text mCipheringIndicator;

        CellInfoOverviewHolder(Component view) {
            mNeighboringCells = (Text) view.findComponentById(ResourceTable.Id_neighboring_cells);
            mNeighboringTotal = (Text) view.findComponentById(ResourceTable.Id_neighboring_number);
            mCipheringIndicatorLabel = (Text) view.findComponentById(ResourceTable.Id_ciphering_indicator_title);
            mCipheringIndicator = (Text) view.findComponentById(ResourceTable.Id_ciphering_indicator);
        }
    }

    class CellInfoAdapter extends BaseItemProvider {
        private final BaseInflaterAdapter<CardItemData> mCardItemDataAdapter;
        private CellInfoOverviewData mOverview;

        CellInfoAdapter(BaseInflaterAdapter<CardItemData> cardItemDataAdapter,
                        CellInfoOverviewData overview) {
            mCardItemDataAdapter = cardItemDataAdapter;
            mOverview = overview;
        }

        void updateCellInfoOverview(CellInfoOverviewData overview) {
            mOverview = overview;
            notifyDataChanged();
        }

        @Override
        public int getCount() {
            return mCardItemDataAdapter.getCount() + 1;
        }

        @Override
        public Object getItem(int pos) {
            if (pos == 0) {
                return null;
            } else {
                return mCardItemDataAdapter.getItem(pos + 1);
            }
        }

        @Override
        public long getItemId(int pos) {
            return pos - 1;
        }

        @Override
        public int getComponentTypeCount() {
            return 2;
        }

        @Override
        public int getItemComponentType(int position) {
            return position == 0 ? 0 : 1;
        }

        @Override
        public Component getComponent(int pos, Component convertView, ComponentContainer parent) {
            if (pos == 0) {
                CellInfoOverviewHolder holder;
                if (convertView == null) {
                    convertView = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_cell_info_overview, parent, false);
                    holder = new CellInfoOverviewHolder(convertView);
                    convertView.setTag(holder);
                } else {
                    holder = (CellInfoOverviewHolder) convertView.getTag();
                }
                if (mOverview.mNeighboringCellsVisibility != -1) {
                    holder.mNeighboringCells.setVisibility(mOverview.mNeighboringCellsVisibility);
                    holder.mNeighboringCells.setText(mOverview.mNeighboringCells);
                }
                if (mOverview.mNeighboringTotalViewVisibility != -1) {
                    holder.mNeighboringTotal.setText("Total :" + mOverview.mNeighboringTotal);
                }
                if (mOverview.mCipheringIndicatorLabelVisibility != -1) {
                    holder.mCipheringIndicatorLabel.setVisibility(mOverview.mCipheringIndicatorLabelVisibility);
                }
                if (mOverview.mCipheringIndicatorVisibility != -1) {
                    holder.mCipheringIndicator.setVisibility(mOverview.mCipheringIndicatorVisibility);
                    holder.mCipheringIndicator.setText(mOverview.mCipheringIndicator);
                }
                return convertView;
            } else {
                return mCardItemDataAdapter.getComponent(pos - 1, convertView, parent);
            }
        }
    }

    private void startservices() {
        serviceIntent = new Intent();
        Operation serviceOperation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.example.ohos_imsi_catcher_detector_development.services")
                .withAbilityName("com.example.ohos_imsi_catcher_detector_development.services.AimsicdService")
                .build();
        serviceIntent.setOperation(serviceOperation);
        startAbility(serviceIntent);
    }

    private AimsicdService aimsicdService;
    // 创建连接回调实例
    private IAbilityConnection connection = new IAbilityConnection() {

        // 连接到Service的回调
        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int i) {
            aimsicdService = new AimsicdService();
            startAbility(serviceIntent);
//            rilExecutor = aimsicdService.getRilExecutor();
            mBound = true;
            //更新UI依赖AsyncTask
            updateUI();
        }

        // 断开与Service连接的回调
        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int i) {
            stopAbility(serviceIntent);
            mBound = false;
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        //销毁
        if (mBound) {
            mBound = false;
            aimsicdService.onDisconnect(serviceIntent);
        }
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (!mBound) {
            // Bind to LocalService
            startservices();
        }
    }

    private void updateUI() {
    }

}
