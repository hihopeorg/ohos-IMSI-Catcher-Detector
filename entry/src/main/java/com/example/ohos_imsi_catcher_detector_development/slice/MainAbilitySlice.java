/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.Fraction.*;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.constants.DrawerMenu;
import com.example.ohos_imsi_catcher_detector_development.drawer.DrawerMenuActivityConfiguration;
import com.example.ohos_imsi_catcher_detector_development.drawer.NavDrawerItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {

    private DrawerMenuActivityConfiguration mNavConf;
    private ListContainer listContainer;

    private DeviceFraction deviceFraction;
    private CellInfoFraction cellInfoFraction;
    private AtCommandFraction atCommandFraction;
    private DbViewerFraction dbViewerFraction;
    private MapFraction mapFraction;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        /*initFraction();*/
        try {
            initListContainer();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initFraction() {
        deviceFraction = new DeviceFraction();
        cellInfoFraction = new CellInfoFraction();
        atCommandFraction = new AtCommandFraction();
        dbViewerFraction = new DbViewerFraction();
        mapFraction = new MapFraction();

    }

    private void initListContainer() throws NotExistException, WrongTypeException, IOException {
        mNavConf = new DrawerMenuActivityConfiguration.Builder(this).build();
        listContainer = (ListContainer) findComponentById(mNavConf.getLeftDrawerId());
        listContainer.setItemProvider(mNavConf.getBaseAdapter());

    }

    void selectDrawerItem(int position) {
        NavDrawerItem selectedItem = mNavConf.getNavItems().get(position);
        String title = selectedItem.getLabel();

        switch (selectedItem.getId()) {
            case DrawerMenu.ID.MAIN.PHONE_SIM_DETAILS:
                openFragment(deviceFraction);
                title = getString(ResourceTable.String_app_name_short);
                break;
            case DrawerMenu.ID.MAIN.NEIGHBORING_CELLS:
                openFragment(cellInfoFraction);
                title = getString(ResourceTable.String_app_name_short);
                break;
            case DrawerMenu.ID.MAIN.AT_COMMAND_INTERFACE:
                openFragment(atCommandFraction);
                title = getString(ResourceTable.String_app_name_short);
                break;
            case DrawerMenu.ID.MAIN.DB_VIEWER:
                openFragment(dbViewerFraction);
                title = getString(ResourceTable.String_app_name_short);
                break;
            case DrawerMenu.ID.APPLICATION.UPLOAD_LOCAL_BTS_DATA:
//                new RequestTask(this, com.secupwn.aimsicd.utils.RequestTask.DBE_UPLOAD_REQUEST).execute("");
                break;
            case DrawerMenu.ID.MAIN.ANTENNA_MAP_VIEW:
                openFragment(mapFraction);
                title = getString(ResourceTable.String_app_name_short);
                break;
        }

    }

    private void openFragment(Fraction fragment) {
        FractionScheduler fractionScheduler = fragment.getFractionAbility().getFractionManager().startFractionScheduler();
        fractionScheduler.add(ResourceTable.Id_content_frame, fragment);
        fractionScheduler.submit();

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
