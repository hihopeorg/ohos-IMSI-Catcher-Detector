/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;

public class AtCommandSlice extends AbilitySlice {

    private Button atCommandExecute;
    private TextField mAtCommand;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_at_command);
        initView();
    }

    private void initView() {
        atCommandExecute = (Button) findComponentById(ResourceTable.Id_btn_execute);
        mAtCommand = (TextField) findComponentById(ResourceTable.Id_at_command);
        atCommandExecute.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mAtCommand.getText() != null) {
                    String command = mAtCommand.getText().toString();
                }
            }
        });
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
