/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.slice;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.services.AimsicdService;
import com.example.ohos_imsi_catcher_detector_development.utils.RadioInfoManagerUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.ElementName;
import ohos.net.*;
import ohos.rpc.IRemoteObject;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SignalInformation;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class DeviceSlice extends AbilitySlice {

    private Text text_gs;
    private String[] phoneType = {"未知", "2G", "3G", "4G"};
    private String[] simState = {"状态未知", "无SIM卡", "被PIN加锁", "被PUK加锁",
            "被NetWork PIN加锁", "已准备好"};
    private Text text_imei;
    private Text text_ril;
    private Text text_country;
    private Text text_id;
    private Text text_name;
    private Text text_imsi;
    private Text text_xil;
    private Text text_gys;
    private Text text_gysdm;
    private Text text_roa;
    private Text text_mode;
    private Text text_data;
    private Text txt_list;
    private boolean mBound;
    private Intent serviceIntent;
    private NetHandle netHandle;
    private NetManager netManager;
    private NetStatusCallback callback;
    public static final String HTTPS = "https://";
    public static final String DEVELOPER = "developer.harmonyos.com/";
    public static final String PATH = "cn/home/";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_device);
        startservices();
        initDeviceSlice();
    }

    private void startservices() {
        serviceIntent = new Intent();
        Operation serviceOperation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.example.ohos_imsi_catcher_detector_development.services")
                .withAbilityName("com.example.ohos_imsi_catcher_detector_development.services.AimsicdService")
                .build();
        serviceIntent.setOperation(serviceOperation);
        startAbility(serviceIntent);
    }

    private AimsicdService aimsicdService;
    // 创建连接回调实例
    private IAbilityConnection connection = new IAbilityConnection() {

        // 连接到Service的回调
        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int i) {
            aimsicdService = new AimsicdService();
            startAbility(serviceIntent);
            mBound = true;
            updateUI();
        }

        // 断开与Service连接的回调
        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int i) {
            stopAbility(serviceIntent);
            mBound = false;
        }
    };

    /**
     * 初始化数据
     */
    private void initDeviceSlice() {
        txt_list = (Text) findComponentById(ResourceTable.Id_txt_list);
        txt_list.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.ohos_imsi_catcher_detector_development")
                        .withAbilityName("com.example.ohos_imsi_catcher_detector_development.ListItem")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });
        text_gs = (Text) findComponentById(ResourceTable.Id_text_phone_info_1);
        text_imei = (Text) findComponentById(ResourceTable.Id_text_phone_info_2);
        text_ril = (Text) findComponentById(ResourceTable.Id_text_phone_info_3);
        text_country = (Text) findComponentById(ResourceTable.Id_text_phone_info_4);
        text_id = (Text) findComponentById(ResourceTable.Id_text_phone_info_5);
        text_name = (Text) findComponentById(ResourceTable.Id_text_phone_info_6);
        text_imsi = (Text) findComponentById(ResourceTable.Id_text_phone_info_7);
        text_xil = (Text) findComponentById(ResourceTable.Id_text_phone_info_8);
        text_gys = (Text) findComponentById(ResourceTable.Id_text_phone_info_9);
        text_gysdm = (Text) findComponentById(ResourceTable.Id_text_phone_info_10);
        text_roa = (Text) findComponentById(ResourceTable.Id_text_phone_info_11);
        text_mode = (Text) findComponentById(ResourceTable.Id_text_phone_info_12);
        text_data = (Text) findComponentById(ResourceTable.Id_text_phone_info_13);
        getTelephonyData();
        getNetServices();

    }

    /**
     * 网络服务
     */
    private void getNetServices() {
        netManager = NetManager.getInstance(this);
        //查询当前是否有可用网络
        boolean isDefaultNet = netManager.hasDefaultNet();
        if (!isDefaultNet) {
            new ToastDialog(getContext()).setText("当前设备无网络信号,请稍后再尝试").show();
            return;
        }

        //获取当前默认的数据网络句柄。
        netHandle = netManager.getDefaultNet();

        //获取当前默认的数据网络状态变化。
        class MmsCallback extends NetStatusCallback {
            @Override
            public void onAvailable(NetHandle netHandle) {
                // 通过setAppNet把后续应用所有的请求都通过该网络进行发送
                netManager.setAppNet(netHandle);

                HttpURLConnection connection = null;
                try {
                    String urlString = new StringBuilder().append(HTTPS).append(DEVELOPER).append(PATH).toString();
                    URL url = new URL(urlString);
                    URLConnection urlConnection = netHandle.openConnection(url, java.net.Proxy.NO_PROXY);
                    if (urlConnection instanceof HttpURLConnection) {
                        connection = (HttpURLConnection) urlConnection;
                    }
                    connection.setRequestMethod("GET");
                    connection.connect();
                    // 之后可进行url的其他操作
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
                // 如果业务执行完毕，可以停止获取
                netManager.removeNetStatusCallback(this);
            }
        }
        /**
         * 配置一个彩信类型的蜂窝网络
         */
        NetSpecifier req = new NetSpecifier.Builder()
                .addCapability(NetCapabilities.NET_CAPABILITY_MMS)
                .addBearer(NetCapabilities.BEARER_CELLULAR)
                .build();

        // 建立数据网络，通过callback获取网络变更状态
        netManager.setupSpecificNet(req, new MmsCallback());
    }

    /**
     * 电话服务
     */
    private void getTelephonyData() {
        //卡1 : 0 ；卡2 :1 卡槽索引号，取值范围为0〜设备支持的最大卡槽索引号
        int slotld = 0;
        // 获取RadioInfoManager对象。
        RadioInfoManager radioInfoManager = RadioInfoManager.getInstance(this);

        //获取注册网络中CS域的RAT。
        RadioInfoManagerUtils managerUtils = new RadioInfoManagerUtils(this);
        String phoneType = managerUtils.getPhoneType(slotld);
        text_gs.setText("手机类型： " + phoneType);

        text_imei.setText("IMEI： " + radioInfoManager.getImei(slotld));
        //软件版本
        text_ril.setText("RIL 版本： " + radioInfoManager.getImeiSv(slotld));

        //获取部署注册网络所在国家/地区的ISO定义的国家/地区代码。
        text_country.setText("国家： " + radioInfoManager.getIsoCountryCodeForNetwork(slotld));

        //运营商ID
        text_id.setText("运营商ID： " + radioInfoManager.getUniqueDeviceId(slotld));

        //获取注册网络的运营商名称。
        text_name.setText("运营商名称： " + radioInfoManager.getOperatorName(slotld));

        //IMSI
        text_imsi.setText("IMSI： " + radioInfoManager.getPrimarySlotId());

        //序列号
        text_xil.setText("序列号： " + radioInfoManager.getPlmnNumeric(slotld));


        //获取设备指定卡槽的制造商代码。
        text_gys.setText("供应商名称： " + radioInfoManager.getOperatorName(slotld));

        //供应商代码
        text_gysdm.setText("供应商代码： " + radioInfoManager.getPlmnNumeric(slotld));

        //检查设备是否在漫游网络中注册。
        text_roa.setText("漫游状态： " + radioInfoManager.isRoaming(slotld));

        //获取指定槽位SIM卡的网络搜索模式。
        text_mode.setText("网络模式： " + radioInfoManager.getNetworkSelectionMode(slotld));

        //获取注册网络的网络状态。
        String networkState = managerUtils.getNetworkState(slotld);
        text_data.setText("数据状态： " + networkState);

        // 获取对应于指定SIM卡的注册网络的信号强度信息列表。
        List<SignalInformation> signalList = radioInfoManager.getSignalInfoList(1);

        // 检查信号信息列表大小。
        new ToastDialog(this).setText("网络信号：" + signalList.size()).show();

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        if (!mBound) {
            // 连接Service
            connectAbility(intent, connection);
        }
        updateUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //销毁
        if (mBound) {
            mBound = false;
            aimsicdService.onDisconnect(serviceIntent);
        }
    }

    //传感器监听功能
    private void updateUI() {
    }
}
