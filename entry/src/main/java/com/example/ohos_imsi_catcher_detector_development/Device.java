package com.example.ohos_imsi_catcher_detector_development;

import com.example.ohos_imsi_catcher_detector_development.slice.DeviceSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class Device extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DeviceSlice.class.getName());
    }
}
