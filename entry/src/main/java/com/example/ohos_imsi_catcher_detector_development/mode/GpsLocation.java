package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;

public class GpsLocation extends RealmObject {

    /**
     * The latitude in degrees
     */
    private double latitude;

    /**
     * The longitude in degrees
     */
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    private double accuracy;
}
