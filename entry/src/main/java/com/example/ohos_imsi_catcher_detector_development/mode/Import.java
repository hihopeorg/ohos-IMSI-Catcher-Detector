package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;

import java.util.Date;

public class Import extends RealmObject {

    public String getDbSource() {
        return dbSource;
    }

    public void setDbSource(String dbSource) {
        this.dbSource = dbSource;
    }

    public String getRadioAccessTechnology() {
        return radioAccessTechnology;
    }

    public void setRadioAccessTechnology(String radioAccessTechnology) {
        this.radioAccessTechnology = radioAccessTechnology;
    }

    public int getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(int mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public int getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public void setMobileNetworkCode(int mobileNetworkCode) {
        this.mobileNetworkCode = mobileNetworkCode;
    }

    public int getLocationAreaCode() {
        return locationAreaCode;
    }

    public void setLocationAreaCode(int locationAreaCode) {
        this.locationAreaCode = locationAreaCode;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public int getPrimaryScramblingCode() {
        return primaryScramblingCode;
    }

    public void setPrimaryScramblingCode(int primaryScramblingCode) {
        this.primaryScramblingCode = primaryScramblingCode;
    }

    public GpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(GpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isGpsExact() {
        return gpsExact;
    }

    public void setGpsExact(boolean gpsExact) {
        this.gpsExact = gpsExact;
    }

    public int getAvgRange() {
        return avgRange;
    }

    public void setAvgRange(int avgRange) {
        this.avgRange = avgRange;
    }

    public int getAvgSignal() {
        return avgSignal;
    }

    public void setAvgSignal(int avgSignal) {
        this.avgSignal = avgSignal;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public Date getTimeFirst() {
        return timeFirst;
    }

    public void setTimeFirst(Date timeFirst) {
        this.timeFirst = timeFirst;
    }

    public Date getTimeLast() {
        return timeLast;
    }

    public void setTimeLast(Date timeLast) {
        this.timeLast = timeLast;
    }

    public Integer getRejCause() {
        return rejCause;
    }

    public void setRejCause(Integer rejCause) {
        this.rejCause = rejCause;
    }

    private String dbSource;
    private String radioAccessTechnology;
    private int mobileCountryCode;
    private int mobileNetworkCode;
    private int locationAreaCode;
    private int cellId;
    private int primaryScramblingCode;
    private GpsLocation gpsLocation;
    private boolean gpsExact;
    private int avgRange;
    private int avgSignal;
    private int samples;
    private Date timeFirst;
    private Date timeLast;
    private Integer rejCause;
}
