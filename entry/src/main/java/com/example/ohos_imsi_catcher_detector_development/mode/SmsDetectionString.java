package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;


public class SmsDetectionString extends RealmObject {

    private String detectionString;

    private String smsType;

    public String getDetectionString() {
        return detectionString;
    }

    public void setDetectionString(String detectionString) {
        this.detectionString = detectionString;
    }

    public String getSmsType() {
        return smsType;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }

}
