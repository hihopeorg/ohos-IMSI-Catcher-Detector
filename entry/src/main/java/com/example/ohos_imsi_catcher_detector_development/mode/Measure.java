package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;

import java.util.Date;

public class Measure extends RealmObject {

    private BaseTransceiverStation baseStation;
    private Date time;
    private GpsLocation gpsLocation;
    private int rxSignal;
    private String radioAccessTechnology;
    private int timingAdvance;
    private boolean submitted;
    private boolean neighbor;

    public BaseTransceiverStation getBaseStation() {
        return baseStation;
    }

    public void setBaseStation(BaseTransceiverStation baseStation) {
        this.baseStation = baseStation;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public GpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(GpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public int getRxSignal() {
        return rxSignal;
    }

    public void setRxSignal(int rxSignal) {
        this.rxSignal = rxSignal;
    }

    public String getRadioAccessTechnology() {
        return radioAccessTechnology;
    }

    public void setRadioAccessTechnology(String radioAccessTechnology) {
        this.radioAccessTechnology = radioAccessTechnology;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }

    public boolean isNeighbor() {
        return neighbor;
    }

    public void setNeighbor(boolean neighbor) {
        this.neighbor = neighbor;
    }
}
