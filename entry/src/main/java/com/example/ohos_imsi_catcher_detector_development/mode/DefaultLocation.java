package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;


public class DefaultLocation extends RealmObject {

    public int getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(int mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public GpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(GpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    private int mobileCountryCode;
    private String country;
    private GpsLocation gpsLocation;
}
