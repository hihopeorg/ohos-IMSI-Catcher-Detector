package com.example.ohos_imsi_catcher_detector_development.mode;

import io.realm.RealmObject;

import java.util.Date;

public class Event extends RealmObject {

    private Date timestamp;
    private String message;
    private GpsLocation gpsLocation;
    private int cellId;
    private int locationAreaCode;
    private int primaryScramblingCode;

    private int dfId;
    private String dfDescription;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(GpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public int getLocationAreaCode() {
        return locationAreaCode;
    }

    public void setLocationAreaCode(int locationAreaCode) {
        this.locationAreaCode = locationAreaCode;
    }

    public int getPrimaryScramblingCode() {
        return primaryScramblingCode;
    }

    public void setPrimaryScramblingCode(int primaryScramblingCode) {
        this.primaryScramblingCode = primaryScramblingCode;
    }

    public int getDfId() {
        return dfId;
    }

    public void setDfId(int dfId) {
        this.dfId = dfId;
    }

    public String getDfDescription() {
        return dfDescription;
    }

    public void setDfDescription(String dfDescription) {
        this.dfDescription = dfDescription;
    }
}
