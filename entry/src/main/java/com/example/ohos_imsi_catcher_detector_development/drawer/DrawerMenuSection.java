
package com.example.ohos_imsi_catcher_detector_development.drawer;


public class DrawerMenuSection implements NavDrawerItem {

    private static final int SECTION_TYPE = 0;
    private int id;
    private String label;

    private DrawerMenuSection() {
    }

    public static DrawerMenuSection create(int id, String label) {
        DrawerMenuSection section = new DrawerMenuSection();
        section.setId(id);
        section.setLabel(label);
        return section;
    }

    @Override
    public int getType() {
        return SECTION_TYPE;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public void setIconId(int icon) {

    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean updateActionBarTitle() {
        return false;
    }
}
