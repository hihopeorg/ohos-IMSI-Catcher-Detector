
package com.example.ohos_imsi_catcher_detector_development.drawer;

import com.example.ohos_imsi_catcher_detector_development.BaseItemProviders.DrawerMenuAdapter;
import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.constants.DrawerMenu;
import ohos.agp.components.BaseItemProvider;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class DrawerMenuActivityConfiguration {

    private DrawerMenuAdapter baseAdapter;
    private int mainLayout;
    private int drawerLayoutId;
    private int leftDrawerId;
    private int[] actionMenuItemsToHideWhenDrawerOpen;
    private List<NavDrawerItem> navItems;

    public DrawerMenuActivityConfiguration(Builder pBuilder) {
        mainLayout = pBuilder.mMainLayout;
        drawerLayoutId = pBuilder.mDrawerLayoutId;
        leftDrawerId = pBuilder.mLeftDrawerId;
        actionMenuItemsToHideWhenDrawerOpen = pBuilder.mActionMenuItemsToHideWhenDrawerOpen;
        navItems = pBuilder.mNavItems;
        baseAdapter = pBuilder.mBaseAdapter;

    }

    public int getDrawerLayoutId() {
        return drawerLayoutId;
    }

    public int getLeftDrawerId() {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "cyp");
        HiLog.info(label, "调试日志：" + leftDrawerId);
        return leftDrawerId;
    }

    public BaseItemProvider getBaseAdapter() {
        return baseAdapter;
    }
    public List<NavDrawerItem> getNavItems() {
        return navItems;
    }

    public void setNavItems(List<NavDrawerItem> navItems) {
        this.navItems = navItems;
    }
    public static class Builder {

        private final Context mContext;
        private int mMainLayout;
        private int mDrawerLayoutId;
        private int mLeftDrawerId;
        private int[] mActionMenuItemsToHideWhenDrawerOpen;
        private List<NavDrawerItem> mNavItems;
        private int mDrawerOpenDesc;
        private int mDrawerCloseDesc;
        private DrawerMenuAdapter mBaseAdapter;

        public Builder(Context pContext) {
            mContext = pContext;
        }

        public Builder mainLayout(int pMainLayout) {
            mMainLayout = pMainLayout;
            return this;
        }

        public Builder drawerLayoutId(int pDrawerLayoutId) {
            mDrawerLayoutId = pDrawerLayoutId;
            return this;
        }

        public Builder leftDrawerId(int pLeftDrawerId) {
            mLeftDrawerId = pLeftDrawerId;
            return this;
        }

        public Builder drawerOpenDesc(int pDrawerOpenDesc) {
            mDrawerOpenDesc = pDrawerOpenDesc;
            return this;
        }

        public Builder drawerCloseDesc(int pDrawerCloseDesc) {
            mDrawerCloseDesc = pDrawerCloseDesc;
            return this;
        }

        public DrawerMenuActivityConfiguration build() throws NotExistException, WrongTypeException, IOException {

            List<NavDrawerItem> menu = new ArrayList<>();
            ResourceManager resourceManager = mContext.getResourceManager();

            menu.add(DrawerMenuSection.create(DrawerMenu.ID.SECTION_MAIN, resourceManager.getElement(ResourceTable.String_main).getString()));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.MAIN.PHONE_SIM_DETAILS,
                    resourceManager.getElement(ResourceTable.String_device_info).getString(),
                    ResourceTable.Media_ic_action_phone, true));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.MAIN.NEIGHBORING_CELLS,
                    resourceManager.getElement(ResourceTable.String_cell_info_title).getString(),
                    ResourceTable.Media_cell_tower, true));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.MAIN.DB_VIEWER,
                    resourceManager.getElement(ResourceTable.String_db_viewer).getString(),
                    ResourceTable.Media_ic_action_storage, true));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.MAIN.ANTENNA_MAP_VIEW,
                    resourceManager.getElement(ResourceTable.String_map_view).getString(),
                    ResourceTable.Media_ic_action_map, false));

            menu.add(DrawerMenuItem.create(DrawerMenu.ID.MAIN.AT_COMMAND_INTERFACE,
                    resourceManager.getElement(ResourceTable.String_at_command_title).getString(),
                    ResourceTable.Media_ic_action_computer, true));

            //Section Settings
            menu.add(DrawerMenuSection.create(DrawerMenu.ID.SECTION_DATABASE_SETTINGS,
                    resourceManager.getElement(ResourceTable.String_database_settings).getString()));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.DATABASE_SETTINGS.RESET_DB,
                    resourceManager.getElement(ResourceTable.String_clear_database).getString(),
                    ResourceTable.Media_ic_action_delete_database, false));

            //Section Application
            menu.add(DrawerMenuSection.create(DrawerMenu.ID.SECTION_APPLICATION,
                    resourceManager.getElement(ResourceTable.String_application).getString()));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.APPLICATION.DOWNLOAD_LOCAL_BTS_DATA,
                    resourceManager.getElement(ResourceTable.String_get_opencellid).getString(),
                    ResourceTable.Media_stat_sys_download_anim0, false));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.APPLICATION.IMPORT_CELL_TOWERS_DATA,
                    resourceManager.getElement(ResourceTable.String_import_cell_towers).getString(),
                    ResourceTable.Media_stat_sys_download_anim0, false));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.APPLICATION.UPLOAD_LOCAL_BTS_DATA,
                    resourceManager.getElement(ResourceTable.String_upload_bts).getString(),
                    ResourceTable.Media_stat_sys_upload_anim0, false));
            menu.add(DrawerMenuItem.create(DrawerMenu.ID.APPLICATION.QUIT,
                    resourceManager.getElement(ResourceTable.String_quit).getString(),
                    ResourceTable.Media_ic_action_remove, false));
            mNavItems = menu;

            if (mMainLayout == 0) {
                this.mainLayout(ResourceTable.Layout_ability_main);
            }

            if (mDrawerLayoutId == 0) {
                this.drawerLayoutId(ResourceTable.Id_drawer_layout);
            }

            if (mLeftDrawerId == 0) {
                this.leftDrawerId(ResourceTable.Id_left_drawer_lit);
            }

            if (mDrawerOpenDesc == 0) {
                this.drawerOpenDesc(ResourceTable.String_drawer_open);
            }

            if (mDrawerCloseDesc == 0) {
                this.drawerOpenDesc(ResourceTable.String_drawer_close);

            }

            if (mBaseAdapter == null) {
                HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "cyp");
                HiLog.info(label, "调试日志：menu=" + menu.size());
//                for (int i = 0; i < menu.size(); i++) {
//                    HiLog.info(label, "getLabel：" + menu.get(i).getLabel());
//                    HiLog.info(label, "getType：" + menu.get(i).getType());
//                    HiLog.info(label, "getId：" + menu.get(i).getId());
//                }
                mBaseAdapter = new DrawerMenuAdapter(mContext, ResourceTable.Layout_drawer_item, menu);
            }

            return new DrawerMenuActivityConfiguration(this);
        }

    }
}
