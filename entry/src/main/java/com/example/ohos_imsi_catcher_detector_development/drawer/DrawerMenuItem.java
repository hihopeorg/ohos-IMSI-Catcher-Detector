/* OHos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.drawer;

import com.example.ohos_imsi_catcher_detector_development.ResourceTable;
import com.example.ohos_imsi_catcher_detector_development.constants.DrawerMenu;

public class DrawerMenuItem implements NavDrawerItem {

    public static final int ITEM_TYPE = 1;

    private int id;
    private String label;
    private int iconId;
    private boolean updateActionBarTitle;

    private boolean showInfoButton;

    private DrawerMenuItem() {
    }

    public static DrawerMenuItem create(int pMenuId, String pLabel, int pIconDrawableId, boolean pUpdateActionBarTitle) {
        return create(pMenuId, pLabel, pIconDrawableId, pUpdateActionBarTitle, true);
    }


    public static DrawerMenuItem create(int pMenuId, String pLabel, int pIconDrawableId, boolean pUpdateActionBarTitle, boolean pIsShowInfoButton) {
        DrawerMenuItem item = new DrawerMenuItem();
        item.setId(pMenuId);
        item.setLabel(pLabel);
        item.setIconId(pIconDrawableId);
        item.setUpdateActionBarTitle(pUpdateActionBarTitle);
        item.setShowInfoButton(pIsShowInfoButton);
        return item;
    }

    private void setShowInfoButton(boolean pIsShowInfoButton) {
        this.showInfoButton = pIsShowInfoButton;
    }
    public boolean isShowInfoButton() {
        return showInfoButton;
    }
    private void setUpdateActionBarTitle(boolean pUpdateActionBarTitle) {
        this.updateActionBarTitle = pUpdateActionBarTitle;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void setIconId(int icon) {
        this.iconId = icon;
    }

    public int getIconId() {
        return iconId;
    }

    public int getId() {
        return id;
    }
    @Override
    public int getType() {
        return ITEM_TYPE;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean updateActionBarTitle() {
        return updateActionBarTitle;
    }

    /**
     * @return Returns a string that describes the menu item
     */
    public int getHelpStringId() {

        switch (id) {

            case DrawerMenu.ID.MAIN.PHONE_SIM_DETAILS:
//                return R.string.help_main_phone_sim_details;
                return ResourceTable.String_help_main_phone_sim_details;

            case DrawerMenu.ID.MAIN.NEIGHBORING_CELLS:
                return ResourceTable.String_help_main_neighboring_cells;

            case DrawerMenu.ID.MAIN.DB_VIEWER:
                return ResourceTable.String_help_main_database_viewer;

            case DrawerMenu.ID.MAIN.ANTENNA_MAP_VIEW:
                return ResourceTable.String_help_main_antenna_map_view;

            case DrawerMenu.ID.MAIN.AT_COMMAND_INTERFACE:
                return ResourceTable.String_help_main_at_command_interface;

            case DrawerMenu.ID.DATABASE_SETTINGS.RESET_DB:
                return ResourceTable.String_help_settings_reset_db;

            case DrawerMenu.ID.APPLICATION.DOWNLOAD_LOCAL_BTS_DATA:
                return ResourceTable.String_help_app_download_local_bts;

            case DrawerMenu.ID.APPLICATION.IMPORT_CELL_TOWERS_DATA:
                return ResourceTable.String_help_app_import_cell_towers;

            case DrawerMenu.ID.APPLICATION.ADD_GET_OCID_API_KEY:
                return ResourceTable.String_help_app_add_get_ocid_api_key;

            case DrawerMenu.ID.APPLICATION.UPLOAD_LOCAL_BTS_DATA:
                return ResourceTable.String_help_app_upload_local_bts;

            case DrawerMenu.ID.APPLICATION.QUIT:
                return ResourceTable.String_help_app_quit;

            default:
                return ResourceTable.String_empty;
        }
    }
}
