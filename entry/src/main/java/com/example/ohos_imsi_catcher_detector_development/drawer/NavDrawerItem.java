
package com.example.ohos_imsi_catcher_detector_development.drawer;

public interface NavDrawerItem {
    int getId();

    String getLabel();

    void setLabel(String label);

    void setIconId(int icon);

    int getType();

    boolean isEnabled();

    boolean updateActionBarTitle();
}
