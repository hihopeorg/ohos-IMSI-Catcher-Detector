package com.example.ohos_imsi_catcher_detector_development.drawer;

public class NavDrawerItemData {
    private static final int SECTION_TYPE = 0;
    private int id;
    private String label;
    private int icon;

    public static int getSectionType() {
        return SECTION_TYPE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
