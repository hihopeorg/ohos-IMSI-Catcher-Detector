/* ohos IMSI-Catcher Detector | (c) AIMSICD Privacy Project
 * -----------------------------------------------------------
 * LICENSE:  http://git.io/vki47 | TERMS:  http://git.io/vki4o
 * -----------------------------------------------------------
 */
package com.example.ohos_imsi_catcher_detector_development.constants;

public class Examples {

    /**
     *  Constants of examples for EventLogItemData
     */
    public static class EVENT_LOG_DATA {
        public static final String LAC = "12345";
        public static final String CID = "543210";
        public static final String PSC = "111";
        public static final String GPSD_LAT = "54.6";
        public static final String GPSD_LON = "25.2";
        public static final String GPSD_ACCU = "100";
        public static final String DF_ID = "2";
    }

}
