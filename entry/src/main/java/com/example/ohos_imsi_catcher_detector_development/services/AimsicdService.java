/*
 *  Copyright (C) 2021 hihope
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.example.ohos_imsi_catcher_detector_development.services;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.rpc.IRemoteObject;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class AimsicdService extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");

    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "AimsicdService::onStart");
        //创建服务
        super.onStart(intent);

    }


    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "AimsicdService::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "AimsicdService::onStop");
        //在Service销毁时调用。Service应通过实现此方法来清理任何资源，如关闭线程、注册的侦听器等。
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
        //在Service创建完成之后调用，该方法在客户端每次启动该Service时都会调用，用户可以在该方法中做一些调用统计、初始化类的操作。
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        //在Ability和Service连接时调用
        return null;
    }

    @Override
    public void onDisconnect(Intent intent) {
        //在Ability与绑定的Service断开连接时调用。
    }
}